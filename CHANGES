Version 1.5:
- CUTEr updated to build with Scilab 5.5 and 5.4

Version 1.4:
- CUTEr updated to build with Scilab 5.4 (minimal version required)

Version 1.3:
- CUTEr updated to build with Scilab 5.3.3

Version 1.2:
- CUTEr source files updated.
- Sizing increased to be able to take most SIF problems into account.
- Constraints problems bugs fixed.
- On line help improved.
- SIF database updated, some of these have been adapted to Scilab (see sif/README).
- Interfaces for optim, fsqp, ipopt, quapro, qld, qpsolve have been added or improved.
- Benchmarking environment has been developped providing automatic run
    of the above solvers against the complete database. 
- Performance profil tools added.
- A license.txt file has been added to give the rights applicable to the different 
  parts of this contribution

Version 1.1:
- Some minor modifications for Scilab 5.2
  fix build on Windows
- Fixed init.h for decoding under Windows

Version 1.0:
- The build process automatically derives the source code for Scilab from the distributed one
    (double precision selection, size selection, conflicting function and common names changed, 
    array initialization, stop instructions replaced by a call to the Scilab error handler)
- The build and load process have been adapted to scilab-5.x 
- The Scilab CUTEr interface now work under Windows
- Sparse tools interfaced
- sifqld function created to test qld against relevant problems in SIF database
- unit tests added see in particular ENGVAL2.tst and LSNNODOC.tst for basic 
    functions test (unconstrained and constrained cases)
