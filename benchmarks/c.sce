//exec ~/NewOptimization/CUTEr-1.x/builder.sce;
exec ~/NewOptimization/CUTEr-1.x/loader.sce;
exec ~/NewOptimization/conmin-2.2/builder.sce
exec ~/NewOptimization/conmin-2.2/loader.sce

  maxit=3000;

  bigbnd=1.e20; tol=1.e-10; epsneq=1e-8; 
lines(72)
stacksize(1e7)
NAME="3PK";
siffile='~/NewOptimization/CUTEr-1.x/sif/'+NAME+'.SIF';
probpath=TMPDIR+'/'+NAME';
CLASS=part(get_classification(siffile),1:3)
ierrdecode=execstr('sifdecode(siffile,probpath)','errcatch')
ierrbuild=execstr('buildprob(probpath)','errcatch')
props=get_problem_sizes(probpath);
ierrsetup=execstr('[XSTART,bl,bu,v,cl,cu,equatn,linear] = csetup(probpath+''/OUTSDIF.d'',[%t %t %f]);','errcatch')

split=cumsum([1,props.nlineq,props.nlinin,props.nnlineq,props.nnlinin]);
Ilineq   = split(1):split(2)-1; //linear equalities
Ilinin   = split(2):split(3)-1; //linear inequalities
Inlineq  = split(3):split(4)-1; //nonlinear equalities
Inlinin  = split(4):split(5)-1; //nonlinear inequalities

bnd=[cl(Inlinin);-cu(Inlinin);cl(Ilinin);-cu(Ilinin)];
bounded = find(bnd>-1d20);
ncon=size(bounded,'*')
isc=[zeros(Inlinin);zeros(Inlinin);ones(Ilinin);ones(Ilinin)];
isc=isc(bounded)


params = init_param();
params=add_param(params,'isc',isc); 
params=add_param(params,'nfdg',1);
params=add_param(params,'delfun',1d-8);
params=add_param(params,'ctmin',1d-8);
params=add_param(params,'iprint',2);
params=add_param(params,'dabfun',1d-10);
pause
[xopt,fopt,gopt,copt,dcopt,ic_res]=conmin_optim(XSTART,conmin_obj,conmin_constr,ncon,bu,bl,maxit,params);
