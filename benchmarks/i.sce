//exec ~/NewOptimization/fsqp-1.x/builder.sce;
exec ~/NewOptimization/CUTEr-1.x/builder.sce;
//exec ~/NewOptimization/fsqp-1.x/loader.sce;
exec ~/NewOptimization/CUTEr-1.x/loader.sce;
exec ~/NewOptimization/sciIpopt/builder.sce

exec ~/NewOptimization/sciIpopt/loader.sce

  maxit=5000;
  iprint=0;
  maxsize=1000;
  bigbnd=1.e20; tol=1.e-12; epsneq=1e-8; 
lines(72)
stacksize(1e7)
NAME="ARGLALE";
//NAME="AGG";
siffile='~/NewOptimization/CUTEr-1.x/sif/'+NAME+'.SIF';
probpath=TMPDIR+'/'+NAME';
CLASS=part(get_classification(siffile),1:3)
ierrdecode=execstr('sifdecode(siffile,probpath)','errcatch')
ierrbuild=execstr('buildprob(probpath)','errcatch')
props=get_problem_sizes(probpath);

ierrsetup=execstr('[XSTART,bl,bu,v,cl,cu,equatn,linear] = csetup(probpath+''/OUTSDIF.d'',[%t %t %f]);','errcatch')

split=cumsum([1,props.nlineq,props.nlinin,props.nnlineq,props.nnlinin]);
Ilineq   = split(1):split(2)-1; //linear equalities
Ilinin   = split(2):split(3)-1; //linear inequalities
Inlineq  = split(3):split(4)-1; //nonlinear equalities
Inlinin  = split(4):split(5)-1; //nonlinear inequalities

constr_lin_type=ones(cl);
constr_lin_type(Ilineq)=1; 
constr_lin_type(Ilinin)=1;

var_lin_type=ones(XSTART);

params = init_param();
params = add_param(params,"hessian_approximation","limited-memory");
params = add_param(params,"linear_solver","mumps");
params = add_param(params,"nlp_lower_bound_inf",-bigbnd);
params = add_param(params,"nlp_upper_bound_inf",bigbnd);
params = add_param(params,"tol",tol);
params = add_param(params,"maxiter",maxit);
params = add_param(params,"constr_viol_tol",epsneq);
nconstr=size(cl,'*');
[c,cjac,indvar,indfun]=ccfsg(XSTART);
sp=[indfun,indvar];
[xopt, fopt, extra]=ipopt(XSTART, ipopt_obj, ipopt_grobj,        ..
			  nconstr, ipopt_cntr, ipopt_grcntr, sp, ..
			  [], [], var_lin_type, constr_lin_type, ..
			  cu, cl, bl, bu, params);
