function [c,ceq,dc,dceq]= byrdsphr_constr(x)
  c=[];
  ceq=[-9.0+x(1)^2+x(2)^2+x(3)^2
	 -9.0+(x(1)-1.0)^2+x(2)^2+x(3)^2];
 
  dc=[];
  dceq=[2*x [2*(x(1)-1.0);x(2);x(3)]];
