function [f,g]= byrdsphr_cost(x)
  f=-x(1)-x(2)-x(3);
g=-ones(3,1);
