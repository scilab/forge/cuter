%Problem :
%*********
%   Source: 
%   R. Fletcher
%   "Practical Methods of Optimization",
%   second edition, Wiley, 1987.

%   SIF input: Ph. Toint, March 1994.

%   classification QOR2-AN-4-4


function [xopt,inform,fopt,gopt,lambda]=fletcher()
   global count;count=[0 0 0 0];
options=optimset('fmincon');

options.GradObj='on';
options.GradConstr='on';
options.MaxFunEvals=1000;
x0=ones([4,1]);
bigbnd=1.e20;
bl=-bigbnd*ones(size(x0));
bu=bigbnd*ones(size(x0));
Ae=[-1 0 1  0
    0 -1 0  1
    0 0  -1 1
    0 0  0  -1]
  be=[1;1;0;1]

[x,fval,exitflag,output,lambda] = fmincon(@fletcher_cost,x0,Ae,Be,[],[],bl,bu,@fletcher_constr,options)

end

function [f,g]=fletcher_cost(x)
  f=x(1)*x(2);
  g=[x(2)
    x(1)
    0
      0];
end



function  [c,ceq,dc,dceq]=fletcher_constr(x)
 t1=(x(1)*x(3)+x(2)*x(4))/(x(1)^2+x(2)^2)
c=[]
ceq=(x(1)*x(3)+x(2)*x(4))^2/(x(1)^2+x(2)^2)-x(3)^2-x(4)^2 +1 
dc=[]
dceq=2*[t1*x(3)-t1^2*x(1)
	  t1*x(4)-t1^2*x(2)
	  t1*x(1)-x(3)
	  t1*x(2)-x(4)]
 

end

