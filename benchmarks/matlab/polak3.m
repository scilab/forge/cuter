%A nonlinear minmax problem in eleven variables.
%Note: the original statement of the problem contains an inconsistant
%      index i.  This has been replaced by 1, assuming a very common typo.
%      But the optimal solution of the resulting problem differs from that
%      quoted in the source. 
%Source: 
%E. Polak, D.H. Mayne and J.E. Higgins,
%"Superlinearly convergent algorithm for min-max problems"
%JOTA 69, pp. 407-439, 1991.
%SIF input: Ph. Toint, Nov 1993.
%classification  LOR2-AN-12-10
%All variables are free

%x=[x1;x2;x3;x4;x5;x6;x7;x8;x9;x10;x11;u]
function [x,fval,exitflag,output,lambda] = polak3()
   global count;count=[0 0 0 0];
  x0=ones([12,1]);
  bl=-1d20*ones([12,1]);
  bu=1d20*ones([12,1]);

  options=optimset('fmincon');

  options.GradObj='on';
  options.GradConstr='on';
  options.MaxFunEvals=2000;

  [x,fval,exitflag,output,lambda] = fmincon(@polak3_cost,x0,[],[],[],[],bl,bu,@polak3_constr,options)
    disp(count);disp(fval)
end


function [f,g]=polak3_cost(x)
  global count;
  count(1:2)=count(1:2)+1;
  f=x(end);
  g=[zeros(11,1);1];
end


function [c,ceq,dc,dceq]=polak3_constr(x)
  global count;
 n=size(x,1);
count(3:4)=count(3:4)+n-1;

  ceq=[];dceq=[];
 
  I=(1:n-1)';
  c=[];dc=[];
  for j=1:n-1
    c=[c;sum( (1 ./I).*exp((x(I)-sin(j-1+2*I)).^2))-x(n)];
    dc=[dc, [(2 ./I).*exp((x(I)-sin(j-1+2*I)).^2).*(x(I)-sin(j-1+2*I)).^3;-1]];
  end
end

