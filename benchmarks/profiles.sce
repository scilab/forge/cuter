mode(-1);
// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
path=get_absolute_file_path('profiles.sce');
load(path+'all.dat')
solvers=['ipopt' 'optimqn','optimgc','fsqpal','fsqpnl','qld','qpsolve', 'quapro']
solvers=solvers(:)'


win=0;
//all problems
R=list();
for s=solvers,execstr('R($+1)=Res_'+s);end
f=scf(win);[min_objs,solver_mins]=perf_profile(R)
ax=gca();ax.title.text='All Problems';

save(path+'figures/All.scg',f)
xs2eps(f,path+'figures/All.eps')
pnames=Res_general.Name;
mputl(pnames+','+solver_mins+','+string(min_objs),path+'best.csv')



//all problems very small size
win=win+1;
sel=find(Res_general.N<100);
R=list();
for s=solvers,execstr('R($+1)=Res_'+s+'(sel)');end
f=scf(win);perf_profile(R)
ax=gca();ax.title.text='All Problems N less than 100';
save(path+'figures/All_very_small.scg',f)
xs2eps(f,path+'figures/All_very_small.eps')

//all problems very small size non lq
win=win+1;
sel=find(Res_general.N<100);
k=grep(Res_general.Type(sel),['QUR' 'QBR' 'QLR']);
sel(k)=[];
R=list();
for s=solvers,execstr('R($+1)=Res_'+s+'(sel)');end
f=scf(win);perf_profile(R)
ax=gca();ax.title.text='Non LQ Problems N less than 100';
save(path+'figures/non_lq_very_small.scg',f)
xs2eps(f,path+'figures/non_lq_very_small.eps')


sel=grep(Res_general.Type,['QUR' 'QBR' 'QLR']);



//all problems small size
win=win+1;
sel=find(Res_general.N<800);
R=list();
for s=solvers,execstr('R($+1)=Res_'+s+'(sel)');end
f=scf(win);perf_profile(R)
ax=gca();ax.title.text='All Problems N less than 800';
save(path+'figures/All_small.scg',f)
xs2eps(f,path+'figures/All_small.eps')


//unconstrained problems
win=win+1;
sel=find(Res_general.nlineq==0&Res_general.nnlineq==0&Res_general.nlinin==0&Res_general.nnlinin==0);
//all unconstrained problems
R=list();
for s=solvers,execstr('R($+1)=Res_'+s+'(sel)');end
f=scf(win);perf_profile(R)
ax=gca();ax.title.text='Unconstrained Problems';
save(path+'figures/Unconstrained.scg',f)
xs2eps(f,path+'figures/Unconstrained.eps')

//small size unconstrained problems
win=win+1;
k=find(Res_general.N(sel)<800);
sel1=sel(k);
R=list();
for s=solvers,execstr('R($+1)=Res_'+s+'(sel1)');end
f=scf(win);perf_profile(R)
ax=gca();ax.title.text='Unconstrained Problems N less than 800';
save(path+'figures/Unconstrained_small.scg',f)
xs2eps(f,path+'figures/Unconstrained_small.eps')

//Linear Quadratic problems
win=win+1;
sel=grep(Res_general.Type,['QUR' 'QBR' 'QLR']);
k=find(Res_general.nnlineq(sel)==0&Res_general.nnlinin(sel)==0);
sel=sel(k);
R=list();
for s=solvers,execstr('R($+1)=Res_'+s+'(sel)');end
f=scf(win);perf_profile(R)
ax=gca();ax.title.text='Linear Quadratic Problems';
save(path+'figures/LQP.scg',f)
xs2eps(f,path+'figures/LQP.eps')

//small size LQP problems
win=win+1;
k=find(Res_general.N(sel)<800);
sel1=sel(k);
R=list();
for s=solvers,execstr('R($+1)=Res_'+s+'(sel1)');end
f=scf(win);perf_profile(R)
ax=gca();ax.title.text='Linear Quadratic Problems N  less than 800';
save(path+'figures/LQP_small.scg',f)
xs2eps(f,path+'figures/LQP_small.eps')
