// #   Source: Problem 9 in
// #   J.J. More', B.S. Garbow and K.E. Hillstrom,
// #   "Testing Unconstrained Optimization Software",
// #   ACM Transactions on Mathematical Software, vol. 7(1), pp. 17-41, 1981.

// #   See also Buckley#28
// #   SIF input: Ph. Toint, Dec 1989.

// #   classification NOR2-AN-3-15
function [xopt,inform,fopt,gopt,lambda]=argauss()
  x0=[0.4;1;0];
  bl=-1d20*ones(x0);
  bu=1d20*ones(x0);

  nf=0; nineqn=0; nineq=0; neqn=15; neq=15; modefsqp=100; miter=500; iprint=0;
  ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
  bigbnd=1.e20; eps=1.e-12; epsneq=1e-8; udelta=0.e0;
  rpar=[bigbnd,eps,epsneq,udelta];
  [xopt,inform,fopt,gopt,lambda]=fsqp(x0,ipar,rpar,[bl,bu],..
				      argauss_cost,argauss_constr,..
				      argauss_grcost,argauss_grconstr)
  disp(count)
  disp(fopt)

endfunction

function f=argauss_cost(i,x),
  f=0
endfunction

function g=argauss_grcost(i,x)
  g=zeros(3,1)
endfunction


function cj=argauss_constr(j,x)
  rhs=[0.0009;0.0044;0.0175;0.0540;0.1295;0.2420;0.3521;0.3989;
     0.3521;0.2420;0.1295;0.0540;0.0175;0.0044;0.0009];
  cj=x(1)*exp(-0.5*x(2)*(0.5*(8-j)-x(3))^2) - rhs(j);
endfunction

function gj=argauss_grconstr(j,x)
  e=-0.5*x(2)*(0.5*(8-j)-x(3))^2
  gj=exp(e)*[1
	     x(1)*e*(-0.5*(0.5*(8-j)-x(3))^2)
	     x(1)*e*(0.5*x(2)*2*(0.5*(8-j)-x(3)))]
endfunction



