


function g=grcost(i,x)
  g=-ones(3,1)
endfunction


function cj=constr(j,x)
  if j==1 then
    cj=-9.0+x(1)^2+x(2)^2+x(3)^2
  else
    cj=-9.0+(x(1)-1.0)^2+x(2)^2+x(3)^2 
  end
endfunction

function cj=grconstr(j,x)
  if j==1 then
    cj=2*x
  else
    cj=[2*(x(1)-1.0);
	x(2)
	x(3)]
  end
endfunction


XSTART=[5;0.0001;-0.0001];
nf=1; nineqn=0; nineq=0; neqn=2; neq=2; modefsqp=100; miter=500; iprint=1;
ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
bigbnd=1.e20; eps=1.e-8; epsneq=1.e-8; udelta=0;
rpar=[bigbnd,eps,epsneq,udelta];
bl=-bigbnd*ones(3,1);
bu=bigbnd*ones(3,1);

[xopt,inform,fopt,gopt,lambda]=fsqp(XSTART,ipar,rpar,[bl,bu],cost,constr,grcost,grconstr)

//http://www.mat.univie.ac.at/~neum/glopt/coconut/Benchmark/Library2_new_v1.html
//the solution is
x1=1/2
x2=sqrt(35/8)//2.0916501 
x3=sqrt(35/8)//2.0916501 
fopt=-1/2-2*sqrt(35/8)//-4.6833001 
