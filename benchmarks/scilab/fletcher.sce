//Problem :
//*********
//   Source: 
//   R. Fletcher
//   "Practical Methods of Optimization",
//   second edition, Wiley, 1987.

//   SIF input: Ph. Toint, March 1994.

//   classification QOR2-AN-4-4


function [xopt,inform,fopt,gopt,lambda]=fletcher()
  global count;count=[0 0 0 0]
  x0=ones(4,1);
  bl=-1d20*ones(x0);
  bu=1d20*ones(x0);

  nf=1; nineqn=0; nineq=4; neqn=1; neq=1; modefsqp=110; miter=500; iprint=0;
  ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
  bigbnd=1.e20; eps=1.e-12; epsneq=1e-8; udelta=0.e0;
  rpar=[bigbnd,eps,epsneq,udelta];
  [xopt,inform,fopt,gopt,lambda]=fsqp(x0,ipar,rpar,[bl,bu],fletcher_cost,fletcher_constr,fletcher_grcost,fletcher_grconstr)
  disp(count)
  disp(fopt)
  return

endfunction

function f=fletcher_cost(i,x)
  global count;count(1)=count(1)+1;
  f=x(1)*x(2)
endfunction

function g=fletcher_grcost(i,x)
  global count;count(2)=count(2)+1;
  g=[x(2)
    x(1)
    0
    0]
endfunction


function cj=fletcher_constr(j,x)
  global count;count(3)=count(3)+1;
  select j
  case 1 then //linear inqualities
     cj=-(x(1)-x(3)-1)
  case 2 then
     cj=-(x(2)-x(4)-1)
  case 3 then
     cj=-(x(3)-x(4))
  case 4 then
    cj=-(x(4)-1)
  case 5 then//non linear equalities
     cj=(x(1)*x(3)+x(2)*x(4))^2/(x(1)^2+x(2)^2)-x(3)^2-x(4)^2 +1 //equality
 
  end
endfunction

function gj=fletcher_grconstr(j,x)
  global count;count(4)=count(4)+1;
  
  select j
  case 1 then
    gj=[-1;0;1;0]
  case 2 then
    gj=[0;-1;0;1]
  case 3 then
    gj=[0;0;-1;1]
  case 4 then
    gj=[0;0;0;-1]
  case 5 then
    t1=(x(1)*x(3)+x(2)*x(4))/(x(1)^2+x(2)^2)
    gj=2*[t1*x(3)-t1^2*x(1)
	  t1*x(4)-t1^2*x(2)
	  t1*x(1)-x(3)
	  t1*x(2)-x(4)]

  end
 
endfunction

