//Problem :
//*********

// #   Source:
// #   K. Schittkowski
// #   "More Test Examples for Nonlinear Programming Codes"
// #   Springer Verlag, Berlin, Lecture notes in economics and 
// #   mathematical systems, volume 282, 1987

// #   SIF input: Michel Bierlaire and Annick Sartenaer, October 1992.
// #              minor correction by Ph. Shott, Jan 1995.

// #   classification QLR2-AN-5-5


function [xopt,inform,fopt,gopt,lambda]=hs268()
  global count;count=[0 0 0 0]
  x0=ones(5,1);
  bl=-1d20*ones(x0);
  bu=1d20*ones(x0);

  nf=1; nineqn=0; nineq=5; neqn=0; neq=0; modefsqp=110; miter=500; iprint=0;
  ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
  bigbnd=1.e20; eps=1.e-12; epsneq=1e-8; udelta=0.e0;
  rpar=[bigbnd,eps,epsneq,udelta];
  [xopt,inform,fopt,gopt,lambda]=fsqp(x0,ipar,rpar,[bl,bu],hs268_cost,hs268_constr,hs268_grcost,hs268_grconstr)
  disp(count)
  disp(fopt)
  return

endfunction

function f=hs268_cost(i,x)
  global count;count(1)=count(1)+1;
  D=[10197	-12454	-1013	1948	329
     -12454	20909	-1733	-4914	-186
     -1013	-1733	1755	1089	-174
     1948	-4914	1089	1515	-22
     329	-186	-174	-22	27];
    B=[-9170
	17099
	-2271
	-4336
	-43];
  f=x'*D*x-2*B'*x+14463.0
endfunction

function g=hs268_grcost(i,x)
  global count;count(2)=count(2)+1;
  D=[10197	-12454	-1013	1948	329
     -12454	20909	-1733	-4914	-186
     -1013	-1733	1755	1089	-174
     1948	-4914	1089	1515	-22
     329	-186	-174	-22	27];
    B=[-9170
	17099
	-2271
	-4336
	-43];
    g=2*(D*x-B)
endfunction


function cj=hs268_constr(j,x)
  global count;count(3)=count(3)+1;
  select j
  case 1 then
    cj=-sum(x)-5
  case 2 then
    cj=-(10*x(1)+10*x(2)-3*x(3)+5*x(4)+4*x(5)-20)
  case 3 then
    cj=-(-8*x(1)+x(2)-2*x(3)-5*x(4)+3*x(5) + 40)
  case 4 then
    cj=-(8*x(1)-x(2)+2*x(3)+5*x(4)-3*x(5) -11)
  case 5 then
    cj=-(-4*x(1)-2*x(2)+3*x(3)-5*x(4)+x(5) +30)
  end
endfunction

function gj=hs268_grconstr(j,x)
    global count;count(4)=count(4)+1;
  select j
  case 1 then
    gj=-ones(x)
  case 2 then
    gj=-[10;10;-3;5;4];
  case 3 then
    gj=-[-8;1;-2;-5;3];
  case 4 then
    gj=-[8;-1;2;5;-3];
  case 5 then
     gj=-[-4;-2;3;-5;1];
  end
endfunction

