//Problem :
//*********

//Source: problem 46 in
//W. Hock and K. Schittkowski,
//"Test examples for nonlinear programming codes",
//Lectures Notes in Economics and Mathematical Systems 187, Springer
//Verlag, Heidelberg, 1981.

//SIF input: Ph.L. Toint, October 1990.

//classification OOR2-AY-5-2


function [xopt,inform,fopt,gopt,lambda]=hs46()
  global count;count=[0 0 0 0]
  x0=[sqrt(2)/2;1.75;0.5;2;2];
  bl=-1d20*ones(x0);
  bu=1d20*ones(x0);

  nf=1; nineqn=0; nineq=0; neqn=2; neq=2; modefsqp=110; miter=500; iprint=0;
  ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
  bigbnd=1.e20; eps=1.e-12; epsneq=1e-8; udelta=0.e0;
  rpar=[bigbnd,eps,epsneq,udelta];
  [xopt,inform,fopt,gopt,lambda]=fsqp(x0,ipar,rpar,[bl,bu],hs46_cost,hs46_constr,hs46_grcost,hs46_grconstr)
  disp(count)
  disp(fopt)
  return

endfunction

function f=hs46_cost(i,x)
  global count;count(1)=count(1)+1;
  f=(x(1)-x(2))^2 + (x(3)-1)^2 + (x(4)-1)^4 + (x(5)-1)^6
endfunction

function g=hs46_grcost(i,x)
  global count;count(2)=count(2)+1;
  g=[2*(x(1)-x(2))
     -2*(x(1)-x(2))
     2*(x(3)-1)
     4*(x(4)-1)^3
     6*(x(5)-1)^5]
endfunction


function cj=hs46_constr(j,x)
  global count;count(3)=count(3)+1;
  if j==1 then
    cj=x(1)^2*x(4) + sin(x(4) - x(5)) - 1;
  else
    cj=x(2) + x(3)^4*x(4)^2 - 2;
  end
endfunction

function gj=hs46_grconstr(j,x)
    global count;count(4)=count(4)+1;
    if j==1 then
      gj=[2*x(1)*x(4)
	  0
	  0
	  x(1)^2+cos(x(4) - x(5))
	  -cos(x(4) - x(5))];
  else
    gj=[0
	1
	4*x(3)^3*x(4)^2 
	2*x(3)^4*x(4)
	0];
  end
endfunction

