function [xopt,inform,fopt,gopt,lambda]=ncvxqp6()
  //http://orfe.princeton.edu/~rvdb/ampl/nlmodels/cute/ncvxqp6.mod
  N=1000;M=floor(N/4);
  x0=0.5*ones(N,1)
  bl=0.1*ones(x0);
  bu=10*ones(x0);

  nf=1; nineqn=0; nineq=0; neqn=0; neq=M; modefsqp=100; miter=500; iprint=0;
  ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
  bigbnd=1.e20; eps=1.e-12; epsneq=1e-8; udelta=0.e0;
  rpar=[bigbnd,eps,epsneq,udelta];
  [xopt,inform,fopt,gopt,lambda]=fsqp(x0,ipar,rpar,[bl,bu],..
				      ncvxqp6_cost,ncvxqp6_constr,..
				      ncvxqp6_grcost,ncvxqp6_grconstr)
  disp(count)
  disp(fopt)

endfunction

function f=ncvxqp6_cost(i,x),
  N=size(x,'*')
  NPLUS=floor(3*N/4)
  i=1:NPLUS
  f=sum((x(i)+x((2*i-1)-floor((2*i-1)/N)*N+1)+x((3*i-1)-floor((3*i-1)/N)*N+1))^2 )
  i=NPLUS+1:N;
  f=f+sum((x(i)+x((2*i-1)-floor((2*i-1)/N)*N+1)+x((3*i-1)-floor((3*i-1)/N)*N+1))^2*i/2)
endfunction

function g=ncvxqp6_grcost(i,x)
  g=zeros(x)
  
endfunction


function cj=ncvxqp6_constr(j,x)
  x(j)+2*x((4*j-1)-floor((4*j-1)/N)*N+1) + 3*x((5*j-1)-floor((5*j-1)/N)*N+1) - 6.0
endfunction

function gj=ncvxqp6_grconstr(j,x)
  N=size(x,'*');
  gj=zeros(x);
  gj(j)=1;
  gj((4*j-1)-floor((4*j-1)/N)*N+1)=2;
  gj((5*j-1)-floor((5*j-1)/N)*N+1)=3;
endfunction

