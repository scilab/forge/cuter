//A nonlinear minmax problem in eleven variables.
//Note: the original statement of the problem contains an inconsistant
//      index i.  This has been replaced by 1, assuming a very common typo.
//      But the optimal solution of the resulting problem differs from that
//      quoted in the source. 
//Source: 
//E. Polak, D.H. Mayne and J.E. Higgins,
//"Superlinearly convergent algorithm for min-max problems"
//JOTA 69, pp. 407-439, 1991.
//SIF input: Ph. Toint, Nov 1993.
//classification  LOR2-AN-12-10
//All variables are free

//x=[x1;x2;x3;x4;x5;x6;x7;x8;x9;x10;x11;u]
function [xopt,inform,fopt,gopt,lambda]=polak3()
  global count;count=[0 0 0 0]
  x0=ones(12,1);
  bl=-1d20*ones(12,1);
  bu=1d20*ones(12,1);

  nf=1; nineqn=11; nineq=11; neqn=0; neq=0; modefsqp=100; miter=500; iprint=0;
  ipar=[nf,nineqn,nineq,neqn,neq,modefsqp,miter,iprint];
  bigbnd=1.e20; eps=1.e-12; epsneq=1e-1; udelta=0.e0;
  rpar=[bigbnd,eps,epsneq,udelta];
  [xopt,inform,fopt,gopt,lambda]=fsqp(x0,ipar,rpar,[bl,bu],polak3_cost,polak3_constr,polak3_grcost,polak3_grconstr)
  disp(count)
  disp(fopt)
  return
//http://www.mat.univie.ac.at/~neum/glopt/coconut/Benchmark/Library2/RES/polak3.res
x_opt=[ 0.00489951
	-0.0580052
	0.0546991
	0.0140571
	-0.0648875
	0.0411324
	0.0316336
	-0.0671048
	0.0242374
	0.0465039
	-0.0638508
	5.933];
f_opt =  5.933

//on obtient un meilleur critere mais 2 contraintes sont violées:
[-0.3529005;-1.3896868;-0.9338009;
 0.0471405; //*
 -0.6116492;-0.6563449; -0.0516007;-1.1917026;-1.2958561;
 0.0532769;  //*
 -0.4445942] 
endfunction

function f=polak3_cost(i,x)
  global count;count(1)=count(1)+1;
  f=x($);
endfunction

function g=polak3_grcost(i,x)
  global count;count(2)=count(2)+1;
  g=[zeros(11,1);1];
endfunction


function cj=polak3_constr(j,x)
  global count;count(3)=count(3)+1;

  n=size(x,'*');
  I=(1:n-1)';
  cj=sum( (1 ./I).*exp((x(I)-sin(j-1+2*I))^2))-x($)
endfunction

function gj=polak3_grconstr(j,x)
    global count;count(4)=count(4)+1;

   n=size(x,'*');
   I=(1:n-1)';
   gj= [(2 ./I).*exp((x(I)-sin(j-1+2*I))^2).*(x(I)-sin(j-1+2*I))^3;-1];
endfunction

