exec ~/NewOptimization/CUTEr-1.x/builder.sce

exec ~/NewOptimization/CUTEr-1.x/loader.sce
path=get_sif_path()+'sif';
//list the files in the sif directory
PBS=stripblanks(basename(listfiles(path+'/*.SIF')));
//sort the names
PBS=gsort(PBS,'g','i');
//PBS='CHANDHEU';
out=mopen('/tmp/out','w');
for k=778:size(PBS,'*')
  mfprintf(out,"%s %d " ,PBS(k),k);
  Path='/tmp/cute_problems/'+PBS(k);
  Entries=['range','group','elfun'];
  l1=link(Path+"/librange.so",Entries);
  ierr=execstr('[XSTART,bl,bu,v,cl,cu,equatn,linear] = csetup(Path+''/OUTSDIF.d'',[%t %t %f]);','errcatch');
  ulink(l1);
  if ierr==0 then mfprintf(out,"ok\n "),else mfprintf(out,"ko %d----------\n",ierr),pause;end
end
mclose(out)
