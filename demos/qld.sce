mode(-1)
// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
mode(3)
path=get_sif_path()+'sif'

sel= sifselect(path,['Q','U','R']); //Select Quadratic unbounded problems
// solve  one with qld
[xopt,lagr,info]=sifqld(path+'/HILBERTB.SIF') ;
fopt=cfn(xopt)



sel= sifselect(path,['Q','L','R']);  //Select Quadratic bounded problems
//solve the seventh with qld
[xopt,lagr,info]=sifqld(path+'/AVGASA.SIF') ; 
fopt=cfn(xopt)
