// ====================================================================
// Copyright (C) INRIA - Serge.Steer@inria.fr
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

path = get_absolute_file_path('cleaner.sce');
rmdir(path+'scilab_en_US_help','s')
mdelete(path+'date_build')
