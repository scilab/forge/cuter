<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0-subset Scilab" xml:id="csetup" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns5="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>17-Mar-2003</pubdate>
  </info>

  <refnamediv>
    <refname>csetup</refname>

    <refpurpose>Set up the data structures for subsequent
    computations</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>[x,bl,bu,v,cl,cu,equatn,linear] = csetup(outsdif_path, options)</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>outsdif_path</term>

        <listitem>
          <para>string the path of the problem relative OUTSDIF.d file (see
          <link linkend="sifdecode"> sifdecode</link> )</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>options</term>

        <listitem>
          <para>a boolean 3-dimensional vector (see below)</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>x</term>

        <listitem>
          <para>real vector, the starting point</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>bl</term>

        <listitem>
          <para>real vector, the lower bounds</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>bu</term>

        <listitem>
          <para>real vector, the upper bounds</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>v</term>

        <listitem>
          <para>initial estimates of the Lagrange multipliers</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>cl</term>

        <listitem>
          <para>lower bounds on the constraints</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>cu</term>

        <listitem>
          <para>upper bounds on the constraints</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>equatn</term>

        <listitem>
          <para>a boolean array whose i-th component is %t if the i-th
          constraint is an equation and %f else</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>linear</term>

        <listitem>
          <para>boolean array whose i-th component is %t if the i-th
          constraint is linear and %f else</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>[x,bl,bu,v,cl,cu]=csetup() returns the starting point in x and the
    lower and upper bounds on the variables in bl and bu, respectively. v
    contains the initial estimates of the Lagrange multipliers, and cl and cu
    contain the lower and upper bounds on the constraints,
    respectively.</para>

    <para>[x,bl,bu,v,cl,cu,equatn,linear]=csetup() also returns equatn, an
    array whose i-th component is 1 if the i-th constraint is an equation and
    0 else, and linear, an array whose i-th component is 1 if the i-th
    constraint is linear and 0 else.</para>

    <para>[x,bl,bu,v,cl,cu]=csetup(options) or
    [x,bl,bu,v,cl,cu,equatn,linear]=csetup(options), where options is a
    3-dimensional boolean vector, permits the reordering of the constraints
    and variables.</para>

    <para>options( 1 ) = efirst, set to %t if the user wishes the general
    equations to occur before the general inequalities.</para>

    <para>options( 2 ) = lfirst, set to %t if the user wishes the general
    linear or affine constraints to occur before the general nonlinear ones.
    If both efirst and lfirst are set to 1, the linear constraints will occur
    before the nonlinear ones. The linear constraints are ordered so that the
    linear equations occur before the linear inequalities. Likewise, the
    nonlinear equations appear before the before the nonlinear
    inequalities.</para>

    <para>options( 3 ) = nvfrst, set to %t if the user wishes the nonlinear
    variables to occur before the linear variables. Any variable which belongs
    to a nontrivial group or to a nonlinear element in a trivial group is
    treated as a nonlinear variable. If the number of variables which appear
    nonlinearly in the objective function (say n_1) is different from the
    number of variables which appear nonlinearly in the constraints (say m_1),
    then the variables are ordered so that the smaller set occurs first. For
    example, if n_1 &lt; m_1, the n_1 nonlinear objective variables occur
    first, followed by the nonlinear Jacobian variables not belonging to the
    first n_1 variables, followed by the linear variables.</para>

    <para>If options is not given, the constraints and variables are not
    reordered.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example">
sifdecode(get_sif_path()+'sif/DEMBO7.SIF',TMPDIR+'/DEMBO7')
buildprob(TMPDIR+'/DEMBO7')
[x,bl,bu,v,cl,cu,equatn,linear] = csetup(TMPDIR+'/DEMBO7/OUTSDIF.d')
  </programlisting>
  </refsection>

  <refsection>
    <title>Authors</title>

    <simplelist type="vert">
      <member>Bruno Durand, INRIA</member>

      <member>Serge Steer, INRIA</member>
    </simplelist>
  </refsection>

  <refsection>
    <title>Bibliography</title>

    <para>Based on CUTEr authored by</para>

    <para>Nicholas I.M. Gould - n.gould@rl.ac.uk - RAL</para>

    <para>Dominique Orban - orban@ece.northwestern.edu - Northwestern</para>

    <para>Philippe L. Toint - Philippe.Toint@fundp.ac.be - FUNDP</para>

    <para>see http://hsl.rl.ac.uk/cuter-www</para>
  </refsection>
</refentry>