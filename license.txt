The CUTEr source files (src/tools directory) are Copyrighted by the
     Council for the Central Laboratory of the Research Councils,
     CERFACS and Facultes Universitaires Notre-Dame de la Paix
     (CCLRC, CERFACS and FUNDP) and is licensed according to

     http://cuter.rl.ac.uk/cuter-www/license.html

The SifDec source files (src/sifdec directory)  are Copyrighted by the
     Council for the Central Laboratory of the Research Councils,
     CERFACS and Facultes Universitaires Notre-Dame de la Paix
     (CCLRC, CERFACS and FUNDP) and is licensed according to
     http://cuter.rl.ac.uk/cuter-www/sifdec/license.html

The mastsif distribution (sif directory) comes from 
     ftp://ftp.numerical.rl.ac.uk/pub/cuter/mastsif_small.tar.gz 
     No explicit license has been found.

The other parts of this toolbox: sci_gateway, macros, benchmarks, help
     and test directories as well as miscellaneous Scilab scripts
     belongs to INRIA and are licensed under the terms of the CeCILL-V2 license: 
     http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
