// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function a=%tabul_c_tabul(a,b)
  fa=getfield(1,a);na=size(fa,'*')
  fb=getfield(1,b);
  a.Tabname=a.Tabname+','+b.Tabname
  for k=3:size(fb,'*')
    fa($+1)=fb(k)
  end
  setfield(1,fa,a)
  for k=3:size(fb,'*')
     a(fa(na+k-2))=b(fb(k))
  end
  
endfunction
