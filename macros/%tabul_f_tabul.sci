// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function a=%tabul_f_tabul(a,b)
  f=getfield(1,a);
  for k=3:size(f,'*')
    a(f(k))=[a(f(k));b(f(k))];
  end
endfunction
