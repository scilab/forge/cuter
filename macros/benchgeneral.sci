// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function R=benchgeneral(varargin)
  nv=size(varargin)
  select nv
  case 0 then //init
    R=benchgeneral([],[],[],[],[],[],[],[],[],[],[],[],[],[],[])
  case 15 then 
    R=mlist(['tabul','Tabname',"Type","Name","N",..
	   "nfree","nfixed","nlower","nupper","nboth","nslack",..
	  "nlinob","nnlinob","nlineq","nnlineq","nlinin","nnlinin"], ...
	    'General',varargin(:))
  end
endfunction
