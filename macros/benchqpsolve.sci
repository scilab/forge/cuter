// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function R=benchqpsolve(varargin)
  nv=size(varargin)
  select nv
  case 0 then //init
    R=benchqpsolve([],[],[],[],[],[])
  case 1 then //failure
    R=benchqpsolve(%nan,%nan,%nan,%nan,%nan,varargin(1))
   case 6 then
    R=mlist(['tabul','Tabname',"Cost","feas","glagn","comp_val","Time","Fail"],'qpsolve',varargin(:))
  else
    error('invalid number of arguments')
  end
endfunction
