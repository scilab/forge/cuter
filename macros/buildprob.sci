// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function buildprob(Path,Pname)
//compile and link the external routines associated with a given
//problem,
  if argn(2)==1 then Pname=basename(Path),end
  global CurrentCUTErProblem
  if part(Path,length(Path))<>'/' then Path=Path+'/',end
  if CurrentCUTErProblem<>[] then
    if Path<>CurrentCUTErProblem(1) then
      l1=CurrentCUTErProblem(2)
      //unlink previously linked problem
      ulink(l1)
    else
      //problem is already built and loaded
      return
    end
  end
  
  wmode=warning("query");warning("off")
  function mprintf(varargin),endfunction
  curdir=pwd()
  chdir(Path)
  Files=['RANGE.f','GROUP.f','ELFUN.f']
  Entries=['range','group','elfun']
  if execstr('libprob=ilib_for_link(Entries,Files,[],''f'')','errcatch')==0 then
    l1=link(Path+libprob,Entries)
    warning(wmode)
    CurrentCUTErProblem=list(Path,l1)
    chdir(curdir)
  else
    chdir(curdir)
    error('Problem while loading the range,group and elfun into Scilab')
  end

endfunction
