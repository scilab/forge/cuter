// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function [feasn,lagn,compn] = checksolution(x,bl,bu,cl,cu,equatn,linear,props)
//http://fr.wikipedia.org/wiki/Conditions_de_Kuhn-Tucker
  n = size(x,'*');
  m = size(equatn,'*'); //number of linear and nonlinear, equality and inequality, constraints
  nlineq = props.nlineq;   //number  of linear equality constraints
  nlinin = props.nlinin;   //number  of linear inequality constraints 
  neq=size(find(equatn),'*')
  
  inf=1.d20;
  //dxmin used for detecting constraint activity
  dxmin = 1.e-8;  

 // Compute functions and derivatives 
  [f,g]    = cofg(x);   // evaluate objective ant its gradient 
  [c,cjac] = ccfg(x);   // evaluate contraints and their gradients

 
  // Check feasibility
  feasn=norm(max(0,[bl-x;x-bu;cl-c;c-cu]),%inf)

  //---------------------------------------
  // Compute the least-squares multiplier 
  // solving g+[eye cjac]*lm  ~ 0
  //         lo<=lm<=up
  //---------------------------------------

  
  // Compute the lower (lo) and upper (up) bounds on lm (Lagrange or KKT
  // multiplier) to use in the least-squares estimation problem
  // boundray constraints on variables
  lo = -%inf*ones(n+m,1);        // a priori no lower bound on lm
  up =  %inf*ones(n+m,1);        // a priori no upper bound on lm
  
  lo(find(bl<=-inf)) = 0;                       // lm >= 0 if no lower bound on x
  up(find(bu>= inf)) = 0;                       // lm <= 0 if no upper bound on x
  lo(find((bl>-inf)&(abs(x-bl)>dxmin))) = 0;    // lm >= 0 if x is inactive at a lower bound
  up(find((bu< inf)&(abs(x-bu)>dxmin))) = 0;    // lm <= 0 if x is inactive at an upper bound

  i=[nlineq+1:nlineq+nlinin nlineq+nlinin+neq+1:m]; //skip equality constraints
  sel=find(cl(i) <= -inf|(abs(c(i)-cl(i)) > dxmin));
  if sel<>[] then  lo(n+i(sel))=0;end          //lm >= 0 if c is inactive at a lower bound
  sel=find(cu(i) >= -inf|(abs(c(i)-cu(i)) > dxmin));
  if sel<>[] then  up(n+i(sel))=0;end          // lm <= 0 if c is inactive at an upper bound
  
  // Form the Jacobian matrix A intervening in the gradient of the
  // Lagrangian
  A = [eye(n,n); cjac];clear cjac
  notfixed=find(lo<>up) 
  A=A(notfixed,:);lo=lo(notfixed);up=up(notfixed);
  lm=zeros(n+m,1);
  // Compute the LS multiplier
  if A<>[] then
    ierr=execstr('[lmf,tmp,info]=qld(A*A'',A*g,[],[],lo,up,0)','errcatch')

    if ierr<>0|info <>0
      mprintf('checksolution:  qld failed ');
      lagn =%nan
      compn=%nan
      return
    end
    //---------------------------------------
    // optimality measure
    //---------------------------------------
  
    lagn = norm(g+A'*lmf,%inf);
    lm(notfixed)=lmf
    clear A
  else
    lagn = norm(g,%inf);
  end
  //---------------------------------------
  // Check complementarity slackness conditions
  //---------------------------------------
 
  
  // v is the vector submitted to the bounds lbs (lower) and ubs (upper) and lmi is the corresponding multiplier
  // all constraints function current value
  v = [x;
       c(nlineq+1:nlineq+nlinin);
       c(nlineq+nlinin+neq+1:m)];
  // all constraints lower bounds
  lbs = [bl;
         cl(nlineq+1:nlineq+nlinin);
         cl(nlineq+nlinin+neq+1:m)];
  // all constraints upper bounds
  ubs = [bu;
         cu(nlineq+1:nlineq+nlinin);
         cu(nlineq+nlinin+neq+1:m)];
  // all Lagrange multiplier
  lmi = [lm(1:n);
         lm(n+nlineq+1:n+nlineq+nlinin);
         lm(n+nlineq+nlinin+neq+1:n+m)];
  compl = zeros(v);

  //find inactive lower bounds on v
  I = find((lbs > -inf) & (abs(lbs-v) > dxmin));  // v(I) is inactive at its lower bounds

  //complementary for inactive lower bounds on v
  if I<>[] then compl(I) = max(compl(I),max(0,-lmi(I))); end   // lmi(I) must be >= 0

  //find inactive  upper bounds on v
  I = find((ubs < inf) & (abs(ubs-v) > dxmin));   // v(I) is inactive at its upper bounds
  
  //complementary for inactive upper bounds on v						  
  if I<>[] then compl(I) = max(compl(I),max(0,lmi(I))); end    // lmi(I) must be <= 0


  // check complementarity

  compn = norm(compl,%inf);

endfunction
