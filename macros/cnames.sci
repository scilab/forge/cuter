// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function [PNAME,XNAMES,GNAMES]=cnames(n,m)
// CNAMES Get problem name, variable names, and constraint names.
// 
//   [pname,xnames,gnames]=cnames returns the problem name in pname,
//   all variable names in xnames, and all constraint names in gnames.
//   The names are stored row-wise in xnames and gnames.
// 
//   [pname,xnames,gnames]=cnames(n,m) gets only the first n variable names
//   and only the first m constraint names.
// 
  select argn(1)
  case 1 then
    PNAME = cnames1()
  case 2 then
    [PNAME,XN] = cnames1()
    XNAMES=[]
    N=length(XN)/10
    if argn(2)>0 then N=max(min(N,n),0),end
    for k=1:N
      XNAMES=[XNAMES;part(XN,1+(k-1)*10:k*10)]
    end
  case 3 then
    [PNAME,XN,GN] = cnames1()
    XNAMES=[]
    N=length(XN)/10
    if argn(2)>0 then N=max(min(N,n),0),end
    for k=1:N
      XNAMES=[XNAMES;part(XN,1+(k-1)*10:k*10)]
    end
    GNAMES=[]
    M=length(GN)/10
    if argn(2)>0 then M=max(min(M,m),0),end
    for k=1:N
      GNAMES=[GNAMES;part(GN,1+(k-1)*10:k*10)]
    end
   
  end
endfunction
