// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function [constr,grconstr,ic]=conmin_constr(x,ct,split)
  if cl==[] then constr=[],grconstr=[],ic=[],return,end
  [C,Cjac] = ccfg(x)
  ic=zeros(C)
  Ilineq   = split(1):split(2)-1 //linear equalities should be empty
  Ilinin   = split(2):split(3)-1 //linear inequalities should be empty
  Inlineq  = split(3):split(4)-1 //nonlinear equalities
  Inlinin  = split(4):split(5)-1 //nonlinear inequalities
  
  f=max(abs(cl),abs(cu));f(f==0)=1;
   
  bnd=[cl(Inlinin);-cu(Inlinin);cl(Ilinin);-cu(Ilinin)];
  bounded = find(bnd>-1d20);
 
 constr=[-C(Inlinin)
	 C(Inlinin)
	 -C(Ilinin)
	 C(Ilinin)]+bnd
 constr=constr./[f(Inlinin);f(Inlinin);f(Ilinin);f(Ilinin)]-ct
 constr=constr(bounded)   
 //ic=find(ct<=constr&constr<=-ct ) 
 kact=find(constr>0)
 ic(kact)=kact

      
 grconstr=[ -Cjac(Inlinin,:)
	    Cjac(Inlinin,:)
	    -Cjac(Ilinin,:)
	    Cjac(Ilinin,:)];
 grconstr=constr./[f(Inlinin);f(Inlinin);f(Ilinin);f(Ilinin)]
 grconstr=grconstr(bounded,:)
 grconstr=grconstr(kact)
endfunction
