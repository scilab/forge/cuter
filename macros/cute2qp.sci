// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function [H,G,f0,A,b,me,ierr]=cute2qp(N,cl,cu,equatn,linear)
  M=size(cl,1)
  ierr=0
  //The criterion is 1/2*X'*H*X+G'*X+f0
  f0=cfn(zeros(N,1));
  [G,A,H] = cgrdh(zeros(N,1),zeros(M,1));
  //The constraints are cl<=A*X+b<=cu
  [b,A] = ccfg(zeros(N,1));
  me=size(find(equatn),'*')
  //Adapt constraints for qld semantics
  //the first me are equality constraints

  if or(cl(1:me)<>cu(1:me)) then 
    mprintf('pb with bounds constraints\n'); 
    pause,
  else
    be=-b(1:me)+cu(1:me)
    Ae=A(1:me,:)
  end
  if and(cl(me+1:$)<=-1d20) then //no lower bounds on constraints
    if and(cu(me+1:$)>=1d20) then //no upper bounds on constraints
      bi=[];Ai=[]
    else //active upper bounds
      bi=-b(me+1:$)+cu(me+1:$);
      Ai=A(me+1:$,:);
    end
  else //active lower bounds
    if and(cu(me+1:$)>=1d20) then //no upper bounds on constraints
      bi=b(me+1:$)-cl(me+1:$);
      Ai=-A(me+1:$,:);
    else //active upper bounds
      bi=[-b(me+1:$)+cu(me+1:$);
	  b(me+1:$)-cl(me+1:$);]
      Ai=[A(me+1:$,:);-A(me+1:$,:)];
    end
  end
  A=[Ae;Ai];b=[be;bi]
endfunction
