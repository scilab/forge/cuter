mode(-1)
// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function cuter_bench(result_file,solvers)
ll=lines()
lines(0)
if argn(2)<2 then
  solvers=['ipopt','optimqn','optimgc','fsqpal','fsqpnl','qld','qpsolve','quapro']
end
if argn(2)<1 then
  result_file='Results_'+date()
end

//Create the problem list
//-----------------------
path=get_sif_path()+'sif';
//list the files in the sif directory
PBS=stripblanks(basename(listfiles(path+'/*.SIF')));
//sort the names
PBS=gsort(PBS,'g','i');

np=size(PBS,1)

//Increase the stack size to be able to solve most of the problems
//----------------------------------------------------------------
stacksize(2d8)

//Check for a restart (goes on with an interrupted benchmark).
//------------------------------------------------------------
//here a previously saved result file is used if it exist.
k1=1;
if listfiles(result_file)<>[] then
  [nams,typs]=listvarinfile(result_file)
  if or(nams=='Res_general') then
    load(result_file);
    n=size(Res_general)
    k1=find(PBS==stripblanks(Res_general.Name(n)))+1;
    for s=solvers
      execstr('Res_'+s+'=Res_'+s+'(1:n)')
    end
  end
end


//Initialize the result data structures
//--------------------------------------
if k1==1 then 
  for s=['general',solvers]
    execstr('Res_'+s+'=bench'+s+'()')
  end
end


// Try to solve the selected problems
//--------------------------------------

for k=k1:np //loop on problems
  mprintf("%d/%d, %s: ",k,np,PBS(k))
  siffile=path+'/'+PBS(k)+'.SIF'
  NAME=basename(siffile)
  probpath=TMPDIR+'/'+NAME
  CLASS=part(get_classification(siffile),1:3)
  //decode the sif file
  ierrdecode=execstr('sifdecode(siffile,probpath)','errcatch')
  if ierrdecode==0 then
    //read the problem size in OUTSDIF.d 
    N=msscanf(mgetl(probpath+'/OUTSDIF.d',1),'%d')
    props=get_problem_sizes(probpath);
    //build the problem and link it
    ierrbuild=execstr('buildprob(probpath)','errcatch')
    if ierrbuild==0 then
	//apply each solver
	for s=solvers //loop on solvers
	  mprintf(" %s",s)
	  //call the test_<solver_name> function
	  execstr('Res_'+s+'=[Res_'+s+';test_'+s+'()]')
	end
	// All solvers applied on current problem
    else //build error
      for s=solvers
	//store 'Build error' in all solvers reports
	execstr('Res_'+s+'=[Res_'+s+';bench'+s+'(''Build error'')]')
      end
    end
  else //decode error
    N=%nan
    props=list([],[],%nan,%nan,%nan,%nan,%nan,%nan,%nan,%nan,%nan,%nan, ...
	       %nan,%nan)
    for s=solvers
      //store 'Decode error' in all solvers reports
      execstr('Res_'+s+'=[Res_'+s+';bench'+s+'(''Decode error'')]')
    end
  end
  //End of problem catenate the results 
  Res_general=[Res_general;benchgeneral(CLASS,NAME,N,props(3:$))];
  
  //Save the current result tables to be able to restart in case of crash
  execstr('save(result_file,Res_general,'+strcat('Res_'+solvers,',')+')')
  mprintf(" done\n")

end //end of loop on problems
lines(ll(2),ll(1))

endfunction
