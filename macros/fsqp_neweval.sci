// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function fsqp_neweval(x,split)
  //the CUTEr constraints are cl<=ccfg(x)<=cu
//the fsqp constraints have to given as  c(x)<=0

  global  fsqp_cost fsqp_grcost fsqp_constr fsqp_grconstr
  time=ureprt(); 
  if time(2)>3600 then
    error(msprintf(_("%s: Time limit (%f) reached\n"),'fsqp',3600))
  end
  fsqp_cost = cfn(x);
  fsqp_grcost = cgr(x);
  
  [C,Cjac] = ccfg(x)

  Ilineq   = split(1):split(2)-1 //linear equalities
  Ilinin   = split(2):split(3)-1 //linear inequalities
  Inlineq  = split(3):split(4)-1 //nonlinear equalities
  Inlinin  = split(4):split(5)-1 //nonlinear inequalities
  
  
  // - 2*nnlinin non-linear inequation       cl(Inlinin) - C(Inlinin)  <=0
  //                                         C(Inlinin)  - cu(Inlinin) <=0
  // - 2*nlinin linear inequation            cl(Ilinin)  - C(Ilinin)   <=0
  //                                         C(Ilinin)   - cu(Ilinin)  <=0
  //
  // - nnlineq non-linear equations          C(Inlineq)  - cu(Inlineq) ==0 (cu(Inlineq)=cl(Inlineq))
  // - nlineq linear equations               C(Ilineq)   - cu(Ilineq)  ==0 (cu(Ilineq)=cl(Ilineq))

  
  bnd=[cl(Inlinin);-cu(Inlinin);cl(Ilinin);-cu(Ilinin);-cu(Inlineq);-cu(Ilineq)];
  bounded = find(bnd>-1d20);
 
 fsqp_constr=[-C(Inlinin)
	       C(Inlinin)
	      -C(Ilinin)
	       C(Ilinin)
	       C(Inlineq)
	       C(Ilineq)]+bnd
  
      
  fsqp_constr=fsqp_constr(bounded)    
      
  fsqp_grconstr=[ -Cjac(Inlinin,:)
		  Cjac(Inlinin,:)
		  -Cjac(Ilinin,:)
		  Cjac(Ilinin,:)
		  Cjac(Inlineq,:)
		  Cjac(Ilineq,:)];
  fsqp_grconstr=fsqp_grconstr(bounded,:)
  set_x_is_new(0); 
endfunction
