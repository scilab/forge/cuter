// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function fj=fsqp_obj(j,x)
  global  fsqp_cost fsqp_grcost fsqp_constr fsqp_grconstr
  if x_is_new() then
     fsqp_neweval(x)
  end
  fj = fsqp_cost 
endfunction
