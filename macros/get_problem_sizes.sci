// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function props=get_problem_sizes(probpath)
//problem must have been decoded previously
//props is a tlist with 13 fields:
//'name' : The problem name
//'nfree',: number of free variables
//'nfixed',: number of fixed variables
//'nlower',: number of lower bounded variables
//'nupper',: number of upper bounded variables
//'nboth',:number of lower and upper bounded variables
//'nslack',
//'nlinob',
//'nnlinob',
//'nlineq',
//'nnlineq',
//'nlinin',
//'nnlinin'
  
  out=mgetl(probpath+'/OUTSDIF.d')
  n=size(out,1)
  while stripblanks(part(out(n),1:8))==''  then n=n-1,end
  P=out(n)
  name=part(P,1:8)
  P=evstr('['+part(P, 9:length(P))+']')
  props=tlist(['probsize','name','nfree','nfixed','nlower','nupper','nboth','nslack','nlinob','nnlinob','nlineq','nnlineq','nlinin','nnlinin'],name,P(1),P(2),P(3),P(4),P(5),P(6),P(7),P(8),P(9),P(10),P(11),P(12))	  
  
endfunction
