// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function p=get_sif_path()
  //return the path where the sif decoder is installed
  t=string(siflib)
  p=t(1)
  p=strsubst(p,'//','/');
  p=part(p,1:length(p)-length('macros')-1)
endfunction
