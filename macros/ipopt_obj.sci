// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function f=ipopt_obj(x)
  time=ureprt(); 
  if time(2)>3600 then
    error(msprintf(_("%s: Time limit (%f) reached\n"),'fsqp',3600))
  end
  f = cfn(x);
endfunction
