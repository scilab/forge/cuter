// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function [f,g,ind] = optim_obj(X,ind)
//external for the Scilab optim function
  time=ureprt(); 
  if time(2)>3600 then
    ind=0
    f=0;g=zeros(X)
    return
  end

  
  g=zeros(X)
  if or(isnan(X)) then ind=0;end
  if or(isinf(X)) then ind=-2;end
  if (ind == 2) then
    f = ufn(X);
    if or(isnan(f))|or(isinf(f)) then ind=-3;end
  end
  if (ind == 3) then
    g = ugr(X)
    if or(isnan(g))|or(isinf(g)) then ind=-4;end
  end
  if (ind == 4) then
    [f,g] = uofg(X)
    if isnan(f)|or(isinf(f))|or(isnan(g))|or(isinf(g)) then ind=-5;end
  end
endfunction

