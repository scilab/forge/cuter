// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function [min_objs,solver_mins]= perf_profile(Res)
//Res is a list of "tabul" data structures one for each solvers
//The   "tabul" data structures  are produced by the  cuter_bench
//     function.
  
  feas_test=0.001;//absolute criterion on feasiblity norm 
  optimality_rtest=1d-5;//relative criterion on optimum cost 
  optimality_atest=1d-5;//absolute criterion on optimum cost 
  
  ns=size(Res); //number of solvers
  np=size( Res(1));//number of problems
  
  //-- Check the Res list consistency  and extract solvers names
  solvers= [];
  for s=1:ns
    solvers=[solvers Res(s).Tabname]
    if size(Res(s))<>np then 
      error("Tabs have incompatible sizes")
    end
  end
  
  //-- Build the performance table (one row per problem)
  Pt=%inf*ones(np,ns);
  Pt1=Pt;
  //for each problem find the best optimal objective over all given solvers
  min_objs=zeros(np,1)//to store the best cost found
  solver_mins=emptystr(np,1)
  for problem=1:np
    //find the best optimal objective over solvers for current problem
    min_obj=%inf;notfailed=[],solver_min=[]
    for s=1:ns
      obj=Res(s).Cost(problem)
      if ~isnan(obj) then 
	if Res(s).feas(problem)<feas_test then //check feasability
	  min_obj=min(min_obj,obj),
	  solver_min=s
	  notfailed=[notfailed s]
	end
      end
    end
    min_objs(problem)=min_obj
    if solver_min<>[] then
      solver_mins(problem)=solvers(solver_min)
    else
      solver_mins(problem)='none'
    end
    if notfailed<>[] then
      // decide which solvers are really successful and store the Time
      succes=[];T=[]
      for s=notfailed
	o=Res(s).Cost(problem)
	if abs(min_obj)<optimality_atest then //absolute test
	  if abs(o)<optimality_atest then 
	    succes=[succes,s];T=[T,Res(s).Time(problem)];
	  end
	else //relative test
	  if (abs(o-min_obj)/min_obj)<optimality_rtest then 
	    succes=[succes,s];T=[T,Res(s).Time(problem)];
	  end
	end
      end
      //normalize and store
      Tmin=min(T(T>0));if Tmin==[] then Tmin=1,end
      Pt1(problem,succes)=T;
      T(T==0)=Tmin
      Pt(problem,succes)= T./Tmin;
    end
  end
  can_solve=min(Pt,'c')
  
  // compute the cumulative distribution of the performance with respect
  // to problems
  step=0.01
  classes=(1:step:50)';

  D=[];
  for s=1:ns
    [ind, occ, info] = dsearch(Pt(:,s),classes-step/2,'c');
    occ=cumsum(occ);
    D=[D occ/np];
  end
  [ind, occ, info] = dsearch(can_solve,classes-step/2,'c');
  occ=cumsum(occ);
  D=[D occ/np];

  colors=[1:7 25  28 31 32 22 16];
  clf();
  plot2d(classes(1:$-1),D,style=colors(1:ns+1))
  a=gca();a.margins(2)=0.2;a.grid=[1 1 1]*color('gray');
  a.x_label.text="normalized time"
  a.y_label.text="cumulated solved problems ratio"
  
  legend([solvers,'can_solve'],-1)

endfunction
