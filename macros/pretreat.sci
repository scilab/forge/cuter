// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function [NC,fixed,notfixed]=pretreat(bl,bu)
//extract boundary constraints
  flag=[];
  N = length(bl);
  fixed = find(bl==bu);
  notfixed = find(bl~=bu);
  NC = N - length(find(bl==-1e20 & bu==1e20));
endfunction
