// ====================================================================
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================


function result2tracefile ( varargin )
  //   Convert the results from the CuteR test set into a trace for performance profiles.
  //
  // Calling Sequence
  //   perftrace = result2trace ( Res , General , filename )
  //   perftrace = result2trace ( Res , General , filename , verbose )
  //
  // Parameters
  //   Res: The element Res(p) is of type "tabul" and represents the results of solver s for problem p.
  //   General:
  //   perftrace: a list of ns items. perftrace(s) is a np x 8 matrix where np is the number of problems
  
  [lhs,rhs]=argn();
  if ( rhs < 3 | rhs > 4 ) then
    errmsg = msprintf(gettext("%s: Unexpected number of input arguments : %d provided while 3 or 4 are expected."), "result2tracefile", rhs);
    error(errmsg)
  end
  
  Res = varargin(1)
  General = varargin(2)
  filename = varargin(3)
  if ( rhs < 4 ) then
    verbose = %f
  else
    verbose = varargin(4)
  end
  
  apifun_typestring ( filename , "filename" , 3 )
  apifun_typeboolean ( verbose , "verbose" , 4 );
  
  //number of problems
  np = size(Res)
  //
  // Open the file
  [fd,err] = mopen( filename , "w" )
  
  // Columns in the trace file
  //  1: modelname
  //  2: modeltype : LP, QP, RMIP, NLP, RMINLP, MIP, and MINLP
  //  3: solvername
  //  4: direction : 0 = min, 1 = max
  //  5: number of variables
  //  6: number of equations
  //  7: solverstatus
  //  8: objective function value
  //  9: time (s)
  // Compute format
  fmt = ""
  // Add model name
  fmt = fmt + "%s,"
  // Add model type
  fmt = fmt + "%s,"
  // Add solvername
  fmt = fmt + "%s,"
  // Add direction
  fmt = fmt + "%s,"
  // Add number of variables
  fmt = fmt + "%s,"
  // Add number of equations
  fmt = fmt + "%s,"
  // Add solverstatus
  fmt = fmt + "%s,"
  // Add objective function value
  fmt = fmt + "%s,"
  // Add time
  fmt = fmt + "%s\n"
  mfprintf(fd,fmt,..
  "modelname", "modeltype", "solvername", "direction", "nbvar" , "nbeq" , "solverstatus", "obj", "resource")
  //
  // Compute format for the data
  fmt = ""
  // Add model name
  fmt = fmt + "%s,"
  // Add model type
  fmt = fmt + "%s,"
  // Add solvername
  fmt = fmt + "%s,"
  // Add direction
  fmt = fmt + "%d,"
  // Add number of variables
  fmt = fmt + "%d,"
  // Add number of equations
  fmt = fmt + "%d,"
  // Add solverstatus
  fmt = fmt + "%d,"
  // Add objective function value
  fmt = fmt + "%.15e,"
  // Add time
  fmt = fmt + "%.15e\n"
  
  for p = 1 : np
    modelname    = General(p).Name;
    if ( verbose ) then
      mprintf("Problem #%d/%d:%s\n", p,np,modelname)
    end
    //
    // Map from CUTEr classification to model type
    objtype = part(General(p).Type,1)
    constype = part(General(p).Type,2)
    smoothtype = part(General(p).Type,3)
    if ( objtype == "N" ) then
      if ( constype == "L" | constype == "X" | constype == "B" | constype == "U" ) then
        modeltype = "LP";
      elseif ( constype == "Q" ) then
        modeltype = "QP";
      elseif ( constype == "O" ) then
        modeltype = "NLP";
      else
        modeltype = "";
      end
    elseif ( objtype == "C" ) then
      if ( constype == "L"  | constype == "X" | constype == "B" | constype == "U" ) then
        modeltype = "LP";
      elseif ( constype == "Q" ) then
        modeltype = "QP";
      elseif ( constype == "O" ) then
        modeltype = "NLP";
      else
        modeltype = "";
      end
    elseif ( objtype == "L" ) then
      if ( constype == "L" | constype == "X" | constype == "B" | constype == "U" ) then
        modeltype = "LP";
      elseif ( constype == "Q" ) then
        modeltype = "QP";
      elseif ( constype == "O" ) then
        modeltype = "NLP";
      else
        modeltype = "";
      end
    elseif ( objtype == "Q" ) then
      if ( constype == "L" | constype == "Q" | constype == "X" | constype == "B" | constype == "U" ) then
        modeltype = "QP";
      elseif ( constype == "O" ) then
        modeltype = "NLP";
      else
        modeltype = "";
      end
    elseif ( objtype == "S" ) then
      // A sum of square : what type of sum of squares : nonlinear or quadratic ?
      if ( constype == "L" | constype == "Q" | constype == "X" | constype == "B" | constype == "U" ) then
        modeltype = "QP";
      elseif ( constype == "O" ) then
        modeltype = "NLP";
      else
        modeltype = "";
      end
    elseif ( objtype == "O" ) then
      modeltype = "NLP";
    else
      modeltype = "";
    end
    solvername   = Res(p).Tabname;
    // TODO : check that all problems are "minimize"
    direction    = 0;
    nbvar = General(p).N;
    nbeq = General(p).nfree + General(p).nfixed + General(p).nlower + ..
    General(p).nupper + General(p).nboth + General(p).nslack + General(p).nlinob +..
    General(p).nnlinob + General(p).nlineq + General(p).nnlineq + General(p).nlinin + General(p).nnlinin
    // Map from CUTEr status to GAMS status
    if ( Res(p).Fail == "" ) then
      solverstatus = 1
    elseif ( Res(p).Fail == "ok" ) then
      solverstatus = 1
    else
      solverstatus = 13
    end
    obj          = Res(p).Cost;
    resource     = Res(p).Time;
    mfprintf(fd,fmt,..
    modelname, modeltype, solvername, direction, nbvar , nbeq , solverstatus, obj, resource)
  end
  err = mclose ( fd )
endfunction

// Generates an error if the given variable is not of type string
function apifun_typestring ( var , varname , ivar )
  if ( type ( var ) <> 10 ) then
    errmsg = msprintf(gettext("%s: Expected string variable for variable %s at input #%d, but got %s instead."),"apifun_typestring", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type boolean
function apifun_typeboolean ( var , varname , ivar )
  if ( type ( var ) <> 4 ) then
    errmsg = msprintf(gettext("%s: Expected boolean but for variable %s at input #%d, got %s instead."),"apifun_typeboolean", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction
// Generates an error if the given variable is not of type real
function apifun_typereal ( var , varname , ivar )
  if ( type ( var ) <> 1 ) then
    errmsg = msprintf(gettext("%s: Expected real variable for variable %s at input #%d, but got %s instead."),"apifun_typereal", varname , ivar , typeof(var) );
    error(errmsg);
  end
endfunction


// GAMS Model and Solver Status Codes 
// http://www.gamsworld.org/performance/status_codes.htm
//MODEL STATUS CODE  	DESCRIPTION
//1 	Optimal
//2 	Locally Optimal
//3 	Unbounded
//4 	Infeasible
//5 	Locally Infeasible
//6 	Intermediate Infeasible
//7 	Intermediate Nonoptimal
//8 	Integer Solution
//9 	Intermediate Non-Integer
//10 	Integer Infeasible
//11 	Licensing Problems - No Solution
//12 	Error Unknown
//13 	Error No Solution
//14 	No Solution Returned
//15 	Solved Unique
//16 	Solved
//17 	Solved Singular
//18 	Unbounded - No Solution
//19 	Infeasible - No Solution
//SOLVER STATUS CODE 	DESCRIPTION
//1 	Normal Completion
//2 	Iteration Interrupt
//3 	Resource Interrupt
//4 	Terminated by Solver
//5 	Evaluation Error Limit
//6 	Capability Problems
//7 	Licensing Problems
//8 	User Interrupt
//9 	Error Setup Failure
//10 	Error Solver Failure
//11 	Error Internal Solver Error
//12 	Solve Processing Skipped
//13 	Error System Failure


