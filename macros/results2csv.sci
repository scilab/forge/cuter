// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function T=results2csv(solvers)
  tabs=['general' solvers];
  nt=size(tabs,'*')
  for k=1:nt
    execstr('R=Res_'+tabs(k))
    f=getfield(1,R)
    Tk=tabul2csv(R);
    Tk=[strcat([tabs(k) emptystr(1,size(f,'*')-3)],';');tabul2csv(R)]
    if k==1 then
      T=Tk
    else
      T=T+';'+Tk
    end
  end

endfunction
