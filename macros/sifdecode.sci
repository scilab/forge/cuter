// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// Copyright (C) 2010 - DIGITEO - Michael Baudin
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function sifdecode(Pathin,Pathout,opts)
//Decodes a problem and creates the external routines associated with it
//Pathin : problem path
//Pathout: resulting files path

  rhs=argn(2)
  if ( fileinfo(Pathin) == [] ) then
    error(msprintf(gettext("%s: File %s does not exist."),"sifdecode",Pathin))
  end
  Pathin=pathconvert(stripblanks(Pathin),%f,%t)
  Pbname=basename(Pathin)
  Pathout=pathconvert(stripblanks(Pathout),%t,%t)
  if getos()=="Windows" then
     Pathin=getshortpathname(Pathin)
     Pathout=getshortpathname(Pathout)
  end
  if ~isdir(Pathout) then mkdir(Pathout),end
  if rhs<3 then opts=[3 0 0 2 1],end
  ierr=execstr('sifdec(Pathin,Pathout,opts)','errcatch')
  if ierr==1111 then
    m=mgetl(Pathout+'OUTMESS')
    m=m(grep(m,'** Exit'))
    error(m)
  elseif ierr<>0 then
    error(lasterror())
  end
  //catenate ELFUN.f and EXTER.f
  mputl([mgetl(Pathout+'ELFUN.f')
	 mgetl(Pathout+'EXTER.f')],Pathout+'ELFUN.f')
endfunction
