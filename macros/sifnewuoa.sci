// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function [fopt,xopt,gopt,report]=sifnewuoa(siffile) 
// The sifoptim function decode the problem given by a '.sif' file,
// built it and runs the optim function on it
  
// siffile:	path of the .sif file
  
// fopt:        criterion value at xopt
// xopt:	point found
// gopt:	gradient at xopt
// report:      row vector of 8 strings.
//              [probname,probsize,#constraints,NITER,fopt,normG ,fail,stop]
  
//Authors S. Steer. Copyright INRIA

  // Optimizer settings


  //decoding and compiling 
  NAME=basename(siffile)
  probpath=TMPDIR+'/'+NAME
  if ~isdir(probpath)
    unix_s('mkdir '+probpath)
  end
  sifdecode(siffile,probpath) //routines are created in the Scilab temporary directory
  buildprob(probpath)
  

  //initialize problem routines data structures
  [XSTART,BL,BU] = usetup(probpath+'/OUTSDIF.d');
  N = size(XSTART,'*')
   [NC,fixed,notfixed] = pretreat(BL,BU)
  function [f]=obj(n,x),f = ufn(x);endfunction
  iprint = 0;
  maxfun = 20000;
  rhoend = 1e-12;
  
  npt = 2*N + 1;
  rhobeg =  0.01;

  xopt = newuoa(npt, XSTART, rhobeg, rhoend, iprint, maxfun,obj);
  fopt=ufn(xopt)
  gopt=ugr(xopt)
  
  //build the report data 
  [time,calls]=ureprt();    
  normG=norm(gopt)
  prec=4.5d-6
  fail = (normG/(abs(fopt)+1) > prec);
  if  fail then c='*',else c=' ',end
  report=[NAME,string([N,NC,calls(1),fopt,normG]),c]
  //remove dynamically loaded entry point created by buildprob
  unlink('siftools')
endfunction
