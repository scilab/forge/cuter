// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function [xopt,lagr,fopt]=sifquapro(siffile,imp) 
// The sifquapro function decode the problem given by a '.sif' file,
// built it and runs the qpsolve function on it
  
// siffile:	path of the .sif file
  
// fopt:        criterion value at xopt
// xopt:	point found
  
//Authors S. Steer. Copyright INRIA

  // Optimizer settings
  //decoding and compiling 
  NAME=basename(siffile)
  probpath=TMPDIR+'/'+NAME
  CLASS=part(get_classification(siffile),1:3)
  
  if and(CLASS<>['QUR' 'QBR' 'QLR' 'LUR' 'LBR' 'LLR']) then
    error(_("The problem Class is out of quapro scope"))
  end

  //Decode the problem,routines are created in propath dir
  sifdecode(siffile,probpath)

  //check if submitted problem is compatible with quapro
  props=get_problem_sizes(probpath)
  if props.nnlineq<>0|props.nnlinin<>0 then 
    error(_("The problem Class is out of quapro scope"))
  end
  
  buildprob(probpath)

  //initialize problem routines data structures
  [XSTART,bl,bu,v,cl,cu,equatn,linear] = csetup(probpath+'/OUTSDIF.d', [%t %t %f]);

  N = size(XSTART,'*')
    
  [H,G,f0,A,b,me]=cute2qp(N,cl,cu,equatn,linear)
  if H==[] then H=zeros(N,N),end
  if argn(2)<2 then
    [xopt,lagr,fopt]=quapro(H,G,A,b,bl,bu,me,XSTART)
  else
    [xopt,lagr,fopt]=quapro(H,G,A,b,bl,bu,me,XSTART,imp)
  end
endfunction
