// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function pbs=sifselect(DB,options)
//DB : path of the directory containing CLASSF.DB database file or the
//     database file full path 
//options : vector of 3 chars [objective constraints regularity]
//          objective may be equal to
//             "n"  no objective function
//             "c"  constant objective function
//             "l"  linear objective function
//             "q"  quadratic objective function
//             "s"  sum of squares objective function
//             "o"  more general objective function
//          constraints may be equal to  
//		"u"  no constraints
//		"x"  constraints as fixed variables
//		"b"  bound constraints
//		"n"  constraints represented by adjacency matrix
//		"l"  linear constraints
//		"q"  quadratic constraints
//		"o"  more general constraints
//           regularity may be equal to  
//              "r" problem is regular
//              "i" problem is irregular
// if options is omitted a dialog will popup to allow manual selection  
//pbs a column vector of matching problem names
  if argn(2)<2 then
    options=DB
    DB=get_sif_path()+'sif/CLASSF.DB'
  else
    //if DB is a dir path, complete it by default database file name
    DB=stripblanks(DB)
    if isdir(DB) then
      if and(part(DB,length(DB))<>['/','\']) then  DB=DB+'/',end
      DB=DB+'CLASSF.DB'
    end
  end
  
  pbs=[]
  
  objc=[ "N" "C" "L" "Q" "S" "O"];
  bndc=[ "U" "X" "B" "N" "L" "Q" "O"];
  regc=["R" "I"];
  if argn(2)==1 then
    objs=['no objective function';'constant';'linear';'quadratic';'sum of squares';'more general']
    obj=list('Objective function',5,objs')
    bnds=['no constraints';'fixed variables';'bounds';'represented by"+...
	 " adjacency matrix';'linear';'quadratic';'more general']
    bnd=list('Constraints',1,bnds')
    regs=['regular';'irregular']
    reg=list('Problem regularity',1,regs')
    rep=x_choices('Select the problems characteristics',list(obj, bnd,reg))
    if rep==[] then return,end
    opt=objc(rep(1))+bndc(rep(2))+regc(rep(3))
  else
    options=convstr(options,'u')
    if and(options(1)<>objc) then
      error('The objective option should be one of the following char: NCLQSO')
    end
    if and(options(2)<>[ "U" "X" "B" "N" "L" "Q" "O"]) then
      error('The objective option should be one of the following char: UXBNLQO')
    end
    if and(options(3)<>["R" "I"]) then
      error('The regularity option should be one of the following char: RI')
    end
  
    opt=convstr(strcat(options),'u')
  end
  
  
  //read in the database file 

  T=mgetl(DB)
  //extract the option part and select those who mach the required options
  sel=find(part(T,10:12)==opt)
  //out put the corresponding problem names
  if sel<>[] then
    pbs=stripblanks(part(T(sel),1:8))
  end
endfunction
