function T=tabul2csv(R)
  f=getfield(1,R)
  for k=3:size(f,'*')
    rk=R(f(k))
    if type(rk)<>10 then
      rk=strsubst(msprintf("%g\n",rk),'.',',')
      rk=strsubst(rk,'Nan','')
    end
    if k==3 then
      T=[f(k);rk]
    else
      T=T+';'+[f(k);rk];
    end
  end
endfunction

      
