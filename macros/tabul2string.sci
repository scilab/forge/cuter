// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function T=tabul2string(R)
  f=getfield(1,R);
  Tabname=R.Tabname
  T=''
  H=[]
  for k=3:size(f,'*')
    Rk=R(f(k))
    if type(Rk)==1 then
      Rk=msprintf("%g\n",Rk)
    elseif type(Rk)==10 then

    else
      error('not yet handled')
    end
    Rk=[f(k);Rk]
    T=T+part(Rk,1:max(length(Rk))+2)
  end
  T=[Tabname;T]
endfunction
