// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at   
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function report=test_conmin()
  //fsqp parameters
  maxit=2000;
  iprint=0;
  maxsize=1000;
  bigbnd=1.e20; eps=1.e-10; epsneq=1e-8; udelta=0;
  
  
  Crash=['ARGAUSS' //nparam<neqn -> crash dans la calcul de phess taille allouee trop faible
	 'EIGENC2'
	 'NLMSURF', //unconstrained problem (why it crahses? size=1024)
	 'ORBIT2' 
	];
  TimeOut=['CAMSHAPE'];
  if modefsqp==110 then TimeOut=[TimeOut;'HADAMARD'];end
  //if or(NAME== Crash) then report=benchconmin('Crash'),return,end
  //if or(NAME==TimeOut) then report=benchconmin('Time out'),return,end

 
  if %t then
    ierrsetup=execstr('[XSTART,bl,bu,v,cl,cu,equatn,linear] = csetup(probpath+''/OUTSDIF.d'',[%t %t %f]);','errcatch')

    if ierrsetup<>0 then report=benchconmin('error:'+lasterror()),return,end
    if size(XSTART,'*')>maxsize then report=benchconmin('N>'+string(maxsize)),return,end

  
   
    split=cumsum([1,props.nlineq,props.nlinin,props.nnlineq,props.nnlinin]);
    Ilineq   = split(1):split(2)-1; //linear equalities
    Ilinin   = split(2):split(3)-1; //linear inequalities
    Inlineq  = split(3):split(4)-1; //nonlinear equalities
    Inlinin  = split(4):split(5)-1; //nonlinear inequalities
      
      
    bnd=[cl(Inlinin);-cu(Inlinin);cl(Ilinin);-cu(Ilinin)];
    bounded = find(bnd>-1d20);
    ncon=size(bounded,'*')
    isc=[zeros(Inlinin);zeros(Inlinin);ones(Ilinin);ones(Ilinin)];
    isc=isc(bounded)

 
    params = init_param();
    params.isc=isc;
    params.nfdg=1;
    params.delfun=1d-4;
    params.ctmin=epsneq
    ierr=execstr('[xopt,fopt,gopt,g_opt,dg_opt,ic_res]=conmin_optim(XSTART,conmin_obj,conmin_cntr,ncon, [bl,bu],maxit,params)','errcatch')

    if ierr<>0 then
      if ierr==1000 then
	report=benchconmin('Not enough memory")
      else
	report=benchconmin('solver error:'+string(ierr))
      end
    else
      [time,calls]=ureprt(); 
      fail=''
      if isnan(fopt)|or(isnan(xopt)) then 
	fail='Nan'
	feas=%nan,glagn=%nan,comp_val=%nan
      else
	[feasn,lagn,compn] = checksolution(xopt,bl,bu,cl,cu,equatn,linear,props)
      end
      report=benchconmin(calls(1),calls(2),fopt,feasn,lagn,compn,time(2),fail)
    end
  else
    report=benchconmin('Out of scope'),
  end
endfunction
