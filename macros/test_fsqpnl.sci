// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at   
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function report=test_fsqpnl()
  //fsqp parameters
  modefsqp=110;
  maxit=2000;
  iprint=0;
  maxsize=800;
  bigbnd=1.e20; eps=1.e-10; epsneq=1e-8; udelta=0;
  
  
  Crash=['ARGAUSS' //nparam<neqn -> crash dans la calcul de phess taille allouee trop faible
	 'EIGENC2'
	 'NLMSURF', //unconstrained problem (why it crahses? size=1024)
	 'ORBIT2' 
	];
  TimeOut=['CAMSHAPE'];
  if modefsqp==110 then TimeOut=[TimeOut;'HADAMARD'];end
  if or(NAME== Crash) then report=benchfsqpnl('Crash'),return,end
  if or(NAME==TimeOut) then report=benchfsqpnl('Time out'),return,end

 
  if %t then
    ierrsetup=execstr('[XSTART,bl,bu,v,cl,cu,equatn,linear] = csetup(probpath+''/OUTSDIF.d'',[%t %t %f]);','errcatch')

    if ierrsetup<>0 then report=benchfsqpnl('error:'+lasterror()),return,end
    if size(XSTART,'*')>maxsize then report=benchfsqpnl('N>'+string(maxsize)),return,end
    if props.nlineq>size(XSTART,'*') then
      report=benchfsqpnl('Out of scope: nlineq>nparams'),return,
    end
     if props.nnlineq>size(XSTART,'*') then
       //this one is not tested by check in cfsqpnl.f
      report=benchfsqpnl('Out of scope: neqn>nparams'),return,
    end
    //let C=ccfg(x),
    //    Ilineq   = 1:nlineq, 
    //    Ilinin   = nlineq+1:nlineq+nlinin
    //    Inlineq  = nlineq+nlinin+1:nlineq+nlinin+nnlineq
    //    Inlinin = nlineq+nlinin+nnlineq+1:nlineq+nlinin+nnlineq+nnlinin
    //The constraints computed by CUTEr are organized as follow:
    // - nlineq linear equations              C(Ilineq)  == cl(Ilineq) == cu(Ilineq)
    // - nlinin linear inequation             cl(Ilinin) <= C(Ilinin)  <= cu(Ilinin)
    // - nnlineq non-linear equations         C(Inlineq) == cl(Inlineq) ==cu(Inlineq)
    // - nnlinin non-linear inequation        cl(Inlinin)<= C(Inlinin) <= cu(Inlinin)
  
    //The  fsqp constraints must be organized in a different way:
    // the transformations from CUTEr to fsqp are made in the functions
    // fsqp_cntr, fsqp_grcntr
    // - 2*nnlinin non-linear inequations      cl(Inlinin) - C(Inlinin)  <=0
    //                                         C(Inlinin)  - cu(Inlinin) <=0
    // - 2*nlinin linear inequation            cl(Ilinin)  - C(Ilinin)   <=0
    //                                         C(Ilinin)   - cu(Ilinin)  <=0
    //
    // - nnlineq non-linear equations          C(Inlineq)  - cu(Inlineq) ==0
    // - nlineq linear equations               C(Ilineq)   - cu(Ilineq)  ==0
   
    split=cumsum([1,props.nlineq,props.nlinin,props.nnlineq,props.nnlinin]);
    Ilineq   = split(1):split(2)-1; //linear equalities
    Ilinin   = split(2):split(3)-1; //linear inequalities
    Inlineq  = split(3):split(4)-1; //nonlinear equalities
    Inlinin  = split(4):split(5)-1; //nonlinear inequalities
      
    

    nineqn=size(find([cl(Inlinin);-cu(Inlinin)]>-1d20),'*');
    nineq= nineqn+size(find([cl(Ilinin);-cu(Ilinin)]>-1d20),'*');
    neqn=props.nnlineq;
    neq = neqn+props.nlineq;
    ipar=  [1, nineqn ,nineq,neqn,neq,modefsqp,maxit, iprint];
    rpar=[bigbnd,eps,epsneq,udelta];
    ierr=execstr('[xopt,inform,fopt,gopt,lambda]=fsqp(XSTART,ipar,rpar,[bl,bu],fsqp_obj,fsqp_cntr,fsqp_grobj,fsqp_grcntr)','errcatch')
//    [xopt,inform,fopt,gopt,lambda]=fsqp(XSTART,ipar,rpar,[bl,bu],fsqp_obj,fsqp_cntr,"grobfd","grcnfd")
    //*          inform= 0:normal termination                                 
    //*          inform= 1:no feasible point found for linear constraints     
    //*          inform= 2:no feasible point found for nonlinear constraints  
    //*          inform= 3:no solution has been found in miter iterations     
    //*          inform= 4:stepsize smaller than machine precision before     
    //*                    a successful new iterate is found                  
    //*          inform= 5:failure in attempting to construct d0              
    //*          inform= 6:failure in attempting to construct d1              
    //*          inform= 7:inconsistent input data                            
    //*	         inform= 8:new iterate essentially identical to previous      
    //*		           iterate, though stopping criterion not satisfied.  
    //*          inform= 9:penalty parameter too large, unable to satisfy
    //*                    nonlinear equality constraint
    
    if ierr<>0 then
      if ierr==1000 then
	report=benchfsqpnl('Not enough memory")
      else
	report=benchfsqpnl('solver error:'+string(ierr))
      end
    else
      [time,calls]=ureprt(); 
      info=['ok','no feasible point found','no feasible point found','maxiter','convergence','d0 construct failed',..
	    'd1 construct failed','invalid input','convergence','penalty parameter too large']
      fail=info(inform+1)
      if or(inform==[1 2 7]) then report=benchfsqpnl(fail),return,end
      if isnan(fopt)|or(isnan(xopt)) then 
	fail='Nan'
	feasn=%nan,lagn=%nan,compn=%nan
      else
	[feasn,lagn,compn] = checksolution(xopt,bl,bu,cl,cu,equatn,linear,props)
      end
      report=benchfsqpnl(calls(1),calls(2),fopt,feasn,lagn,compn,time(2),fail)
    end
  else
    report=benchfsqpnl('Out of scope'),
  end
endfunction
