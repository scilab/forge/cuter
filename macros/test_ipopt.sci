// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at   
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function report=test_ipopt()

  maxit=8000;
  iprint=0;
  maxsize=1000;
  bigbnd=1.e20; tol=1.e-12; epsneq=1e-8; 
  
  
  Crash=['ARGAUSS' //nparam<neqn -> crash dans la calcul de phess taille allouee trop faible
	 'ORBIT2'
	'CHANDHEU' //why?
	];
  if or(NAME== Crash) then report=benchipopt('Crash'),return,end

  if %t then
    ierrsetup=execstr('[XSTART,bl,bu,v,cl,cu,equatn,linear] = csetup(probpath+''/OUTSDIF.d'',[%t %t %f]);','errcatch')

    if ierrsetup<>0 then report=benchipopt('error:'+lasterror()),return,end
    //if size(XSTART,'*')>maxsize then report=benchipopt('N>'+string(maxsize)),return,end
    if props.nlineq+ props.nnlineq>size(XSTART,'*') then
      //dans ce cas ipopt crash bizaremment, sans message d'erreur
      report=benchconmin('Out of scope: nlineq>nparams'),return,
    end
    split=cumsum([1,props.nlineq,props.nlinin,props.nnlineq,props.nnlinin]);
    Ilineq   = split(1):split(2)-1; //linear equalities
    Ilinin   = split(2):split(3)-1; //linear inequalities
    Inlineq  = split(3):split(4)-1; //nonlinear equalities
    Inlinin  = split(4):split(5)-1; //nonlinear inequalities
    constr_lin_type=ones(cl);
    constr_lin_type(Ilineq)=1;
    constr_lin_type(Ilinin)=1;
    var_lin_type=ones(XSTART)
    
    params = init_param();
    params = add_param(params,"hessian_approximation","limited-memory");
    params = add_param(params,"linear_solver","mumps");
    params = add_param(params,"nlp_lower_bound_inf",-bigbnd);
    params = add_param(params,"nlp_upper_bound_inf",bigbnd);
    params = add_param(params,"tol",tol);
    params = add_param(params,"max_iter",maxit);
    params = add_param(params,"constr_viol_tol",epsneq);
    params = add_param(params,'acceptable_tol',10*tol);
    nconstr=size(cl,'*');
    
    [c,cjac,indvar,indfun]=ccfsg(XSTART);
    sp=[indfun,indvar];

    ierr=execstr('[xopt, fopt, extra]=ipopt(XSTART, ipopt_obj, ipopt_grobj,nconstr, ipopt_cntr, ipopt_grcntr, sp,[], [], var_lin_type, constr_lin_type,cu, cl, bl, bu, params);','errcatch')

    
    if ierr<>0 then
      if ierr==1000 then
	report=benchipopt('Not enough memory")
      else
	report=benchipopt('solver error:'+string(ierr))
      end
    else
      fail=''
      inform=extra.status;
      [time,calls]=ureprt(); 

      if isnan(fopt)|or(isnan(xopt)) then 
	fail='Nan'
	feasn=%nan,lagn=%nan,compn=%nan
      else
	if extra.status>1 then fail='status='+string(extra.status),end
	ierr=execstr('[feasn,lagn,compn] = checksolution(xopt,bl,bu,cl,cu,equatn,linear,props)','errcatch')
	if ierr<>0 then 
	  feasn=%nan,lagn=%nan,compn=%nan
	end
      end
      report=benchipopt(calls(1),calls(2),fopt,feasn,lagn,compn,time(2),fail)
    end
  else
    report=benchipopt('Out of scope'),
  end
endfunction
