// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function report=test_qpsolve()
  maxsize=2500;
  //using the calling scope variables
  if or(CLASS==['QUR' 'QBR' 'QLR'])&props.nnlineq==0&props.nnlinin==0 then
    ierrsetup=execstr('[XSTART,bl,bu,v,cl,cu,equatn,linear] = csetup(probpath+''/OUTSDIF.d'',[%t %t %f]);','errcatch')
    if ierrsetup<>0 then report=benchqpsolve('error:'+lasterror()),return,end
    if size(XSTART,'*')>maxsize then report=benchqpsolve('N>'+string(maxsize)),return,end
 
    if execstr('[H,G,f0,A,b,me]=cute2qp(N,cl,cu,equatn,linear)','errcatch')<>0 then
      report=benchqpsolve('cute2qp error:'+lasterror()),
      return
    end
    if and(bl<=-1d20) then bl1=[],else bl1=bl,end
    if and(bu>=1d20) then bu1=[],else bu1=bu,end
    timer(); 
    ierr=execstr('[xopt,iact,iter,fopt]=qpsolve(H,G,A,b,bl1,bu1,me);','errcatch')
    t=timer()
    if ierr==0 then
      fail=''
      ierr=execstr('[feasn,lagn,compn] = checksolution(xopt,bl,bu,cl,cu,equatn,linear,props)','errcatch')
      if ierr<>0 then 
	feasn=%nan,lagn=%nan,compn=%nan
      end
      report=benchqpsolve(fopt,feasn,lagn,compn,t,fail),
    else
      report=benchqpsolve('error:'+lasterror()),
    end
  else
    report=benchqpsolve('Out of scope'),
  end
endfunction
