// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function report=test_quapro()
 maxsize=2500;
  //using the calling scope variables
  if or(CLASS==['QUR' 'QBR' 'QLR' 'LUR' 'LBR' 'LLR'])&props.nnlineq==0&props.nnlinin==0 then
    ierrsetup=execstr('[XSTART,bl,bu,v,cl,cu,equatn,linear] = csetup(probpath+''/OUTSDIF.d'',[%t %t %f]);','errcatch')
    if ierrsetup<>0 then report=benchquapro('error:'+lasterror()),return,end
    if size(XSTART,'*')>maxsize then report=benchquapro('N>'+string(maxsize)),return,end

    if execstr('[H,G,f0,A,b,me]=cute2qp(N,cl,cu,equatn,linear)','errcatch')<>0 then
      report=benchqld('cute2qp error:'+lasterror()),
      return
    end
    N=size(XSTART,'*')

 
    if H==[] then H=zeros(N,N),end
    timer(); 
    ierr=execstr('[xopt,lagr,fopt]=quapro(H,G,A,b,bl,bu,me,XSTART);','errcatch')
    t=timer()
    if ierr==0 then
      fail=''
      ierr=execstr('[feasn,lagn,compn] = checksolution(xopt,bl,bu,cl,cu,equatn,linear,props)','errcatch')
      if ierr<>0 then 
	feasn=%nan,lagn=%nan,compn=%nan
      end
      report=benchquapro(fopt,feasn,lagn,compn,t,fail),
    else
      report=benchquapro('error:'+lasterror()),
    end
  else
    report=benchquapro('Out of scope'),
  end
endfunction
