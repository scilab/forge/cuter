// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function [PNAME,VNAME]=unames(n)
// UNAMES Get problem name and variable names.
// 
//   [pname,xnames]=unames returns the problem name in pname and
//   all variable names in xnames.  The variable names are stored
//   row-wise in xnames.
// 
//   [pname,xnames]=unames(n) gets only the first n variable names.
// 

  if (argn(1)==1) then
    PNAME = unames1()
  else   
    [PNAME,VN] = unames1()
    VNAME=[]
    N=length(VN)/10
    if argn(2)>0 then N=max(min(N,n),0),end
    for k=1:N
       VNAME=[VNAME;part(VN,1+(k-1)*10:k*10)]
    end
  end
endfunction

