// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function []=unlink(fct)
//unlink an entry point
  [test,ilink] = c_link(fct);
  while test
    ulink(ilink);
    [test,ilink] = c_link(fct);
  end
endfunction
