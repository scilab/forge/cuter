// ====================================================================
// Copyright (C) INRIA - Serge Steer
// Copyright (C) DIGITEO - Allan CORNET - 2012
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function builder_gw_tools()

mode(-1)
mprintf(_('---------- Building the TOOLS gateway\n'));
path=get_absolute_file_path('builder.sce');

src_files=['intcreprt.f'
     'intccfg.f'
     'intccfsg.f'
     'intccifg.f'
     'intccifsg.f'
     'intcdh.f'
     'intcsh.f'
     'intcdimen.f'
     'intcdimsh.f'
     'intcdimsj.f'
     'intcfn.f'
     'intcgrdh.f'
     'intcsgrsh.f'
     'intcgr.f'
     'intcidh.f'
     'intcish.f'
     'intcnames.f'
     'intcofg.f'
     'intcprod.f'
     'intcsetup.f'
     'intcvarty.f'
     'intubandh.f'
     'intudh.f'
     'intudimen.f'
     'intudimsh.f'
     'intufn.f'
     'intugrdh.f'
     'intugrsh.f'
     'intugr.f'
     'intunames.f'
     'intuofg.f'
     'intuprod.f'
     'intureprt.f'
     'intusetup.f'
     'intush.f'
     'intuvarty.f']';

if newest(path+['date_build','builder.sce',src_files])==1 then
  return
end

libs='../../src/tools/libsiftools' //must be a relative path

//C2F used because function gateways are written in Fortran
table =['creprt' 'C2F(intcreprt)'
  'ccfg'   'C2F(intccfg)'
  'ccfsg'  'C2F(intccfsg)'
  'ccifg'  'C2F(intccifg)'
  'ccifsg' 'C2F(intccifsg)'
  'cdh'    'C2F(intcdh)'
  'csh'    'C2F(intcsh)'
  'cdimen' 'C2F(intcdimen)'
  'cdimsh' 'C2F(intcdimsh)'
  'cdimsj' 'C2F(intcdimsj)'
  'cfn'    'C2F(intcfn)'
  'cgrdh'  'C2F(intcgrdh)'
  'csgrsh'  'C2F(intcsgrsh)'
  'cgr'    'C2F(intcgr)'
  'cidh'   'C2F(intcidh)'
  'cish'   'C2F(intcish)'
  'cnames' 'C2F(intcnames)'
  'cofg'   'C2F(intcofg)'
  'cprod'  'C2F(intcprod)'
  'csetup' 'C2F(intcsetup)'
  'cvarty' 'C2F(intcvarty)'
  'ubandh' 'C2F(intubandh)'
  'udh'    'C2F(intudh)'
  'udimen' 'C2F(intudimen)'
  'udimsh' 'C2F(intudimsh)'
  'ufn'    'C2F(intufn)'
  'ugrdh'  'C2F(intugrdh)'
  'ugrsh'  'C2F(intugrsh)'
  'ugr'    'C2F(intugr)'
  'unames' 'C2F(intunames)'
  'uofg'   'C2F(intuofg)'
  'uprod'  'C2F(intuprod)'
  'ureprt' 'C2F(intureprt)'
  'usetup' 'C2F(intusetup)'
  'ush'    'C2F(intush)'
  'uvarty' 'C2F(intuvarty)'];
try
  tbx_build_gateway('tools',table,..
        src_files, ..
        path,libs,"","")
  mputl(sci2exp(getdate()),path+'date_build')
catch
  mprintf("%s\n",lasterror())
end
endfunction

builder_gw_tools();
clear builder_gw_tools;
