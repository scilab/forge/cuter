      subroutine intccfg(fname)
c     Author: S. Steer, Copyright INRIA
c     [c,cjac]=ccfg(x,jtrans)
      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
      logical GRAD,JTRANS
c
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      if(.not.checkrhs(fname,1,2)) return
      if(.not.checklhs(fname,1,2)) return
      if(.not.getrhsvar(1,'d', Mx, Nx, lX)) return
      if (mx*nx.ne.NVAR) then
         buf='X does not match current problem dimension'
         call error(999)
         return
      endif
C  Decide if the Jacobian is required.  If yes, decide if its transpose
C  is required.
         grad = .false.
         jtrans = .false.
         if ( lhs .eq. 2 ) grad = .true.
         if ( grad .and. rhs .eq. 2 ) then
            if(.not.getrhsvar(1,'d', Mi, Ni, li)) return
            i=stk(li)
            if ( i .ne. 0 ) jtrans = .true.
         end if
C  Set row and column dimensions of CJAC based on JTRANS.
         if ( .not. jtrans ) then
            lc1 = m
            lc2 = nvar
         else
            lc1 = nvar
            lc2 = m
         end if
c
C  Get the values of the constraint functions, and possibly their gradients.
C

      if(.not.createvar(rhs+1,'d', M, 1, lC)) return
      if(grad) then
         if(.not.createvar(rhs+2,'d', lc1, lc2, lCJAC)) return
      endif
      call ccfg(nvar,m,stk(lx),m,stk(lc),jtrans,lc1,lc2,stk(lcjac),grad)
      lhsvar(1)=rhs+1
       if(grad) lhsvar(2)=rhs+2
      return
      end
