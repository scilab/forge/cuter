      subroutine intccfsg(fname)
c     Author: S. Steer,Copyright INRIA
c     [c,cjac,indvar,indfun]=ccfsg(x)
      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar,checklhs,checkrhs
      logical GRAD
c
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      if(.not.checkrhs(fname,1,1)) return
      if(.not.checklhs(fname,1,4)) return
      if(.not.getrhsvar(1,'d', Mx, Nx, lX)) return
      if (mx*nx.ne.NVAR) then
         buf='X does not match current problem dimension'
         call error(999)
         return
      endif
C  Decide if the Jacobian is required.  
      grad = .false.
      if ( lhs .ge. 2 ) grad = .true.
C  Get the values of the constraint functions, and possibly their gradients.
C
      if (M.eq.0) then
         if(.not.createvar(rhs+1,'d', 0, 0, lC)) return
         if(grad) then
            if(.not.createvar(rhs+2,'d', 0, 0, lCJAC)) return
            if(.not.createvar(rhs+3,'i', 0, 0, lindvar)) return
            if(.not.createvar(rhs+4,'i', 0, 0, lindfun)) return
         endif
      else
         if(.not.createvar(rhs+1,'d', M, 1, lC)) return
         if(grad) then
            call cdimsj(nnzj)
            nnzj=nnzj-NVAR
            if(.not.createvar(rhs+2,'d', nnzj, 1, lCJAC)) return
            if(.not.createvar(rhs+3,'i', nnzj, 1, lindvar)) return
            if(.not.createvar(rhs+4,'i', nnzj, 1, lindfun)) return
         endif
         call ccfsg(nvar,m,stk(lx),m,stk(lc),nnzj,nnzj,stk(lcjac),
     $        istk(lindvar),istk(lindfun),grad)
      endif
      lhsvar(1)=rhs+1
      if(grad) then
         lhsvar(2)=rhs+2
         if ( lhs .ge. 3 ) lhsvar(3)=rhs+3
         if ( lhs .ge. 4 ) lhsvar(4)=rhs+4
      endif
      return
      end
      
