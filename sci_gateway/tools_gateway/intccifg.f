      subroutine intccifg(fname)
c     Author: S. Steer, Copyright INRIA
c     [ci,gci]=ccifg(x,i)
      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
      logical GRAD,JTRANS
c
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      if(.not.checkrhs(fname,2,2)) return
      if(.not.checklhs(fname,1,2)) return
      if(.not.getrhsvar(1,'d', Mx, Nx, lX)) return
      if (mx*nx.ne.NVAR) then
         buf='X does not match current problem dimension'
         call error(999)
         return
      endif
      if(.not.getrhsvar(2,'d', Mi, Ni, lI)) return
      i=stk(lI)
      if( (i .lt. 1) .or. (i .gt. m) ) then
         buf = 'invalid constraint index.'
         call error(9999)
         return
      endif
c
      if(lhs.eq.2) then
          if(.not.createvar(3,'d', 1, 1, lCI)) return
          if(.not.createvar(4,'d', NVAR, 1, lCGI)) return
          call ccifg( nvar, i, stk(lx), stk(lci), stk(lcgi), .true. )
          lhsvar(1)=3
          lhsvar(2)=4

       else
         if(.not.createvar(3,'d', 1, 1, lCI)) return
         lcgi=lci
         call ccifg( nvar, i, stk(lx), stk(lci), stk(lcgi), .false. )
         lhsvar(1)=3
      endif

      return
      end
