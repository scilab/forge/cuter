      subroutine intcdh(fname)
c     Author: S. Steer, Copyright INRIA
c     H=udh(X,V)
      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
c
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      if(.not.checkrhs(fname,2,2)) return
      if(.not.checklhs(fname,1,1)) return
      if(.not.getrhsvar(1,'d', Mx, Nx, lX)) return
      if (mx*nx.ne.NVAR) then
         buf='X does not match current problem dimension'
         call error(999)
         return
      endif
      if(.not.getrhsvar(2,'d', Mv, Nv, lV)) return
      if (mv*nv.ne.M) then
         buf='Incorrect dimension'
         call error(999)
         return
      endif

      if(.not.createvar(3,'d', NVAR, NVAR, lDH)) return
      CALL CDH( NVAR, M,stk(lX),M,stk(lV),NVAR, stk(lDH))
      lhsvar(1)=3
      return
      end
