      subroutine intcdimen(fname)
c     Author: S. Steer, Copyright INRIA
c     [n,m]=cdimen
      include 'stack.h'

      logical getrhsvar,createvar
      logical checklhs,checkrhs

      external getrhsvar,createvar, checklhs,checkrhs
      character fname*(*)
      COMMON / toolssize / NVAR,M
      save / toolssize /
c
      rhs=max(0,rhs)
      if(.not.checklhs(fname,1,2)) return
      if(.not.checkrhs(fname,0,0)) return
      if(.not.createvar(1,'d', 1, 1, lN)) return
      stk(lN)=NVAR
      if(.not.createvar(2,'d', 1, 1, lM)) return
      stk(lM)=M
c
      lhsvar(1)=1
      lhsvar(2)=2
      return
      end
