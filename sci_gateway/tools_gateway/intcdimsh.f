      subroutine intcdimsh(fname)
c     Author: S. Steer, Copyright INRIA
c     nnzh=cdimsh()
      include 'stack.h'

      logical getrhsvar,createvar
      logical checklhs,checkrhs

      external getrhsvar,createvar, checklhs,checkrhs
      character fname*(*)
      COMMON / toolssize / NVAR,M
      save / toolssize /
c
      rhs=max(0,rhs)
      if(.not.checklhs(fname,1,1)) return
      if(.not.checkrhs(fname,0,0)) return
      if(.not.createvar(1,'d', 1, 1, lN)) return
      call CDIMSH( NNZH  )
      stk(lN)=NNZH
c
      lhsvar(1)=1
      return
      end
