      subroutine intcfn(fname)
c     Author: S. Steer, Copyright INRIA
c     [F,C]=cfn(X)
      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
c
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      if(.not.checkrhs(fname,1,1)) return
      if(.not.checklhs(fname,1,2)) return
      if(.not.getrhsvar(1,'d', Mx, Nx, lX)) return
      if (mx*nx.ne.NVAR) then
          buf='X does not match current problem dimension'
         call error(999)
         return
      endif
      if(.not.createvar(2,'d', 1, 1, lF)) return
      if(.not.createvar(3,'d', M, 1, lC)) return
      CALL CFN( NVAR,M, stk(lX), stk(lF),M,stk(lC) )
      lhsvar(1)=2
      lhsvar(2)=3
      return
      end
