      subroutine intcgrdh(fname)
c     Author: S. Steer, Copyright INRIA
c     [g,cjac,H]=cgrdh(X,V,options)
      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
      logical JTRANS,GRLAGF
c
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      if(.not.checkrhs(fname,2,3)) return
      if(.not.checklhs(fname,1,3)) return
      if(.not.getrhsvar(1,'d', Mx, Nx, lX)) return
      if (mx*nx.ne.NVAR) then
         buf='X does not match current problem dimension'
         call error(999)
         return
      endif
      if(.not.getrhsvar(2,'d', Mv, Nv, lV)) return
      if (mv*nv.ne.M) then
         buf='Incorrect dimension'
         call error(999)
         return
      endif
      JTRANS = .FALSE.
      GRLAGF = .FALSE.
      if(rhs.eq.3) then
         if(.not.getrhsvar(3,'b', Mo, No, lO)) return
         if (mo*no.ne.2) then
            buf='Incorrect dimension'
            call error(999)
            return
         endif
         JTRANS = istk(lO).ne.0
         GRLAGF = istk(lO+1).ne.0
      endif
      if ( .not. jtrans ) then
         lc1 = m
         lc2 = nvar
      else
         lc1 = nvar
         lc2 = m
      end if
      if(.not.createvar(rhs+1,'d', NVAR, 1, lG)) return
      if(.not.createvar(rhs+2,'d', lc1, lc2, lCJAC)) return
      if(.not.createvar(rhs+3,'d', NVAR, NVAR, lDH)) return

      CALL CGRDH( NVAR,M,stk(lX),GRLAGF,M,stk(lV),stk(lG),JTRANS, 
     $     LC1,LC2,stk(lCJAC),NVAR,stk(lDH))

      lhsvar(1)=rhs+1
      lhsvar(2)=rhs+2
      lhsvar(3)=rhs+3
      return
      end
