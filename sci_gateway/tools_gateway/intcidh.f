      subroutine intcidh(fname)
c     Author: S. Steer, Copyright INRIA
c     Hi=cidh(x,i)
      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
c
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      if(.not.checkrhs(fname,2,2)) return
      if(.not.checklhs(fname,1,1)) return
      if(.not.getrhsvar(1,'d', Mx, Nx, lX)) return
      if (mx*nx.ne.NVAR) then
         buf='X does not match current problem dimension'
         call error(999)
         return
      endif
      if(.not.getrhsvar(2,'d', Mi, Ni, lI)) return
      i=stk(lI)
      if( (i .lt. 0) .or. (i .gt. m) ) then
         buf = 'invalid constraint index.'
         call error(9999)
         return
      endif

      if(.not.createvar(3,'d', NVAR, NVAR, lDH)) return
      CALL CIDH( NVAR,stk(lX),I,NVAR, stk(lDH))

      lhsvar(1)=3
      return
      end
