      subroutine intcish(fname)
c     Author: S. Steer, Copyright INRIA
c     Hi=cish(x,i)
      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
c
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      if(.not.checkrhs(fname,2,2)) return
      if(.not.checklhs(fname,3,3)) return
      if(.not.getrhsvar(1,'d', Mx, Nx, lX)) return
      if (mx*nx.ne.NVAR) then
          buf='X does not match current problem dimension'
         call error(999)
         return
      endif
      if(.not.getrhsvar(2,'d', Mi, Ni, lI)) return
      i=stk(lI)
      if( (i .lt. 0) .or. (i .gt. m) ) then
         buf = 'invalid constraint index.'
         call error(9999)
         return
      endif
      if (i.eq.0) then
         call cdimsh(nnzh)
         if(.not.createvar(rhs+1,'d', nnzh,1, lH)) return
         if(.not.createvar(rhs+2,'i', nnzh,1, lIRNH)) return
         if(.not.createvar(rhs+3,'i', nnzh,1, lICNH)) return
         call CISH   ( NVAR , stk(lX) , I, NNZH  ,NNZH ,
     $        stk(lH) , istk(lIRNH) , istk(lICNH))
      else
         buf='not yet implemented'
         call error(998)
         return
C$$$         nnz=???

C$$$         if(.not.createvar(rhs+1,'d',  nnz,1, lH)) return
C$$$         if(.not.createvar(rhs+2,'i',  nnz,1, lIRNH)) return
C$$$         if(.not.createvar(rhs+3,'i',  nnz,1, lICNH)) return
C$$$         call CISH   ( NVAR , stk(lX) , I, NNZ  ,NNZ ,
C$$$     $        stk(lH) , istk(lIRNH) , istk(lICNH))
      endif
      lhsvar(1)=rhs+1
      lhsvar(2)=rhs+2
      lhsvar(3)=rhs+3

      return
      end
