      subroutine intcprod(fname)
c     Author: S. Steer, Copyright INRIA
c     q=cprod(x,v,p [,goth])

      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
c
      LOGICAL           GOTH
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      rhs=max(0,rhs)
      if(.not.checkrhs(fname,3,4)) return
      if(.not.checklhs(fname,1,1)) return

      if(.not.getrhsvar(1,'d', Mx, Nx, lX)) return
      if (mx*nx.ne.NVAR) then
         buf='X does not match current problem dimension'
         call error(999)
         return
      endif
      if(.not.getrhsvar(2,'d', Mv, Nv, lv)) return
      if (mv*nv.ne.M) then
         buf='Incorrect dimension'
         call error(999)
         return
      endif
      if(.not.getrhsvar(3,'d', Mp, Np, lP)) return
      if (mp*np.ne.NVAR) then
         buf='Incorrect dimension for P'
         call error(999)
         return
      endif
      GOTH=.false.
      if (rhs.eq.4) then
         if(.not.getrhsvar(4,'b', Mg, Ng, lG)) return
         if(istk(lG).ne.0) GOTH=.true.
      endif

      if(.not.createvar(rhs+1,'d', NVAR, 1, lQ)) return
      call CPROD( NVAR, M,GOTH, stk(lX), M, stk(lV), stk(lP), stk(lQ) )
      lhsvar(1)=rhs+1
      return
      end
