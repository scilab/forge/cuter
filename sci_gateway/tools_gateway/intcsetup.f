      subroutine intcsetup(fname)
c     [x,bl,bu,v,cl,cu,equatn,linear]=csetup(file_path,options)
c     Author: S. Steer, Copyright INRIA
      include 'stack.h'

      logical getrhsvar,createvar
      logical checklhs,checkrhs
      integer getcuterr
      logical  efirst,lfirst,nvfrst
      external getrhsvar,createvar, checklhs,checkrhs
      external getcuterr
      character fname*(*)
      character*8 subname
      character*128 path
      integer prbdat,mode(2)
      COMMON / toolssize / NVAR,M
      save / toolssize /
c
      if(.not.checklhs(fname,1,8)) return
      if(.not.checkrhs(fname,1,2)) return
      if(.not.getrhsvar(1,'c', Mp, Np, lP)) return
c
      efirst = .false.
      lfirst = .false.
      nvfrst = .false.
      if(rhs.eq.2) then
         if(.not.getrhsvar(2,'b', Mo, No, lO)) return
         efirst = istk(lO).ne. 0
         lfirst = istk(lO+1).ne. 0
         nvfrst = istk(lO+2).ne. 0
      endif
c
      mode(1)=1
      mode(2)=0
      path=cstk(lP:lP+min(mp*np-1,127))
      subname='range'//char(0)
      call setrangefonc(subname, ierr)
      if(ierr.ne.0) then
         buf='The range function is not linked'
         call error(1007)
         return
      endif
      subname='elfun'//char(0)
      call setelfunfonc(subname, ierr)
      if(ierr.ne.0) then
         buf='The elfun function is not linked'
         call error(1007)
         return
      endif
      subname='group'//char(0)
      call setgroupfonc(subname, ierr)
      if(ierr.ne.0) then
         buf='The group function is not linked'
         call error(1007)
         return
      endif
      prbdat=0
      call clunit(prbdat,path,mode)
      if(err.gt.0) then
         call error(err)
         return
      endif

c     read the problem dimension to be able to allocate
      READ(prbdat,'(2i8)') NVAR,NG
      rewind(prbdat)

      MMAX=NG
      if(.not.createvar(3,'d', MMAX, 3, l1)) goto 10 
      if(.not.createvar(4,'i', MMAX, 2, l2)) goto 10 
      
      if(.not.createvar(5,'d', NVAR, 1, lX)) goto 10
      if(.not.createvar(6,'d', NVAR, 1, lBL)) goto 10 
      if(.not.createvar(7,'d', NVAR, 1, lBU)) goto 10 

      CALL CSETUP(prbdat,wte,NVAR,M,stk(lX),stk(lBL),stk(lBU),NVAR,
     *     istk(l2),istk(l2+MMAX),stk(l1),stk(l1+MMAX),stk(L1+2*MMAX),
     *     MMAX,EFIRST, LFIRST, NVFRST )

      ierr=getcuterr()
      if (ierr.ne.0) then
         if (ierr.eq.-1) then
            buf='csetup: Overdimensionned problem'
         elseif(ierr.eq.1) then
            buf='csetup: the problem includes general constraints'
         elseif(ierr.eq.2) then
            buf='csetup: the problem uses no variables'
         elseif(ierr.eq.3) then
            buf='csetup: the problem is vacuous'
         else
            write(buf,'(''csetup: declared arrays to small '',i3)') ierr
         endif
         call clunit(-prbdat,path,mode)
         call error(1008)
         return
      endif
      if(.not.createvar(8,'d', M, 1, lV)) goto 10 
      call dcopy(M,stk(l1),1,stk(lV),1)
      if(.not.createvar(9,'d', M, 1, lCL)) goto 10 
      call dcopy(M,stk(l1+MMAX),1,stk(lCL),1)
      if(.not.createvar(10,'d', M, 1, lCU)) goto 10 
      call dcopy(M,stk(l1+2*MMAX),1,stk(lCU),1)

      if(.not.createvar(11,'b', M, 1, lE)) goto 10 
c     under windows equatn may contain negative values
      do i=0,M-1
         if (istk(l2+i).eq.0) then
            istk(lE+i)=0
         else
            istk(lE+i)=1
         endif
      enddo
c     call icopy(M,istk(l2),1,istk(lE),1)


      if(.not.createvar(12,'b', M, 1, lL)) goto 10 
c     under windows linear may contain negative values
      do i=0,M-1
         if (istk(l2+MMAX+i).eq.0) then
            istk(lL+i)=0
         else
            istk(lL+i)=1
         endif
      enddo
c     call icopy(M,istk(l2+MMAX),1,istk(lL),1)

      lhsvar(1)=5
      lhsvar(2)=6
      lhsvar(3)=7
      lhsvar(4)=8
      lhsvar(5)=9
      lhsvar(6)=10
      lhsvar(7)=11
      lhsvar(8)=12

 10   call clunit(-prbdat,path,mode)
      return
      end
