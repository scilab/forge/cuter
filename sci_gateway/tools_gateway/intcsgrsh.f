      subroutine intcsgrsh(fname)
c     Author: S. Steer, Copyright INRIA
c     [cjac,indvar,indfun,H,ICNH,IRNH]=csgrsh(x,v)

      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
      integer nnz
      integer GRLAGF
c
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      rhs=max(0,rhs)
      if(.not.checkrhs(fname,2,3)) return
      if(.not.checklhs(fname,1,6)) return
      if(.not.getrhsvar(1,'d', Mx, Nx, lX)) return
      if (mx*nx.ne.NVAR) then
         buf='X does not match current problem dimension'
         call error(999)
         return
      endif
      if(.not.getrhsvar(2,'d', Mv, Nv, lV)) return
      if (mv*nv.ne.M) then
         buf='Incorrect dimension'
         call error(999)
         return
      endif
      GRLAGF=0
      if (rhs.eq.3) then
         if(.not.getrhsvar(3,'d', Mg, Ng, lg)) return
         if (mg*ng.ne.1) then
            buf='Incorrect dimension'
            call error(999)
            return
         endif
         GRLAGF=stk(lg)
      endif 
      call cdimsj(nnzj)
      if(.not.createvar(rhs+1,'d', nnzj, 1, lCJAC)) return
      if(.not.createvar(rhs+2,'i', nnzj, 1,  lindvar)) return
      if(.not.createvar(rhs+3,'i', nnzj,  1, lindfun)) return
      call CDIMSH(nnz)
      if(.not.createvar(rhs+4,'d', nnz, 1, lH)) return
      if(.not.createvar(rhs+5,'i', nnz, 1, lIR)) return
      if(.not.createvar(rhs+6,'i', nnz, 1, lIC)) return
      call CSGRSH(NVAR, M, stk(lX),GRLAGF,M,stk(lV),
     $     NNZJ,NNZJ,stk(lCJAC),istk(lindvar),istk(lindfun),
     $     nnz,nnz, stk(lh),istk(lIR), istk(lIC))
      lhsvar(1)=rhs+1
      lhsvar(2)=rhs+2
      lhsvar(3)=rhs+3
      lhsvar(4)=rhs+4
      lhsvar(5)=rhs+5
      lhsvar(6)=rhs+6
      return
      end
