      subroutine intufn(fname)
c     Author: S. Steer, Copyright INRIA
c     [F]=ufn(X)
      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
c
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      if(.not.checkrhs(fname,1,1)) return
      if(.not.checklhs(fname,1,1)) return
      if(.not.getrhsvar(1,'d', Mx, Nx, lX)) return
      if (mx*nx.ne.NVAR) then
         buf='X does not match current problem dimension'
         call error(999)
         return
      endif
      if(.not.createvar(2,'d', 1, 1, lF)) return
      CALL UFN( NVAR, stk(lX), stk(lF) )
      lhsvar(1)=2
      return
      end
