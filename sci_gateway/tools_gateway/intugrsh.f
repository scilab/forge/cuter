      subroutine intugrsh(fname)
c     Author: S. Steer, Copyright INRIA
c     [G,H,ICNH,IRNH] = ugrsh(X)
      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
c
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      if(.not.checkrhs(fname,1,1)) return
      if(.not.checklhs(fname,1,4)) return
      if(.not.getrhsvar(1,'d', Mx, Nx, lX)) return
      if (mx*nx.ne.NVAR) then
         buf='X does not match current problem dimension'
         call error(999)
         return
      endif
      call udimsh(nnzh)
      if(.not.createvar(2,'d', nvar, 1, lg)) return
      if(.not.createvar(3,'d', nnzh,1, lh)) return
      if(.not.createvar(4,'i', nnzh,1, lirnh)) return
      if(.not.createvar(5,'i', nnzh,1, licnh)) return
      
      ldh=nnzh
      call ugrsh( nvar, stk(lx), stk(lg),nnzh, ldh , stk(lh),
     $     istk(lirnh),istk(licnh))
      lhsvar(1)=2
      lhsvar(2)=3
      lhsvar(3)=4
      lhsvar(4)=5
      return
      end
