      subroutine intunames(fname)
c     Author: S. Steer, Copyright INRIA
c     [PNAME,XNAMES]=unames()
      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs,cresmat4
      external getrhsvar,createvar, checklhs,checkrhs,cresmat4
c
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      rhs=max(0,rhs)
      if(.not.checkrhs(fname,0,0)) return
      if(.not.checklhs(fname,1,2)) return

      if(.not.createvar(1,'c', 10, 1, lPNAME)) return
      if(.not.createvar(2,'c', NVAR, 10, lXNAMES)) return
      CALL UNAMES( NVAR, cstk(lPNAME:), cstk(lXNAMES:))
      lhsvar(1)=1
      lhsvar(2)=2
      return
      end

