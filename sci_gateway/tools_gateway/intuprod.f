      subroutine intuprod(fname)
c     Author: S. Steer, Copyright INRIA
c     q=uprod(x,p [,goth])

      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
c
      LOGICAL           GOTH
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      rhs=max(0,rhs)
      if(.not.checkrhs(fname,2,3)) return
      if(.not.checklhs(fname,1,1)) return

      if(.not.getrhsvar(1,'d', Mx, Nx, lX)) return
      if (mx*nx.ne.NVAR) then
         buf='X does not match current problem dimension'
         call error(999)
         return
      endif
      if(.not.getrhsvar(2,'d', Mp, Np, lP)) return
      if (mp*np.ne.NVAR) then
         buf='Incorrect dimension for P'
         call error(999)
         return
      endif
      GOTH=.false.
      if (rhs.eq.3) then
         if(.not.getrhsvar(3,'b', Mg, Ng, lG)) return
         if(istk(lG).ne.0) GOTH=.true.
      endif

      if(.not.createvar(rhs+1,'d', NVAR, 1, lQ)) return
      call UPROD( NVAR, GOTH, stk(lX), stk(lP), stk(lQ) )
      lhsvar(1)=rhs+1
      return
      end
