      subroutine intureprt(fname)
c     Author: S. Steer, Copyright INRIA
c     [time,calls]=ureprt();    
      include 'stack.h'
c
      logical getrhsvar,createvar,checklhs,checkrhs
      external getrhsvar,createvar, checklhs,checkrhs
c
      REAL               CALLS( 4 )
      REAL               TIME( 2 )
      character fname*(*)
      common / toolssize / NVAR,M
      save / toolssize /
c     
      rhs=max(0,rhs)
      if(.not.checkrhs(fname,0,0)) return
      if(.not.checklhs(fname,1,2)) return
      if(.not.createvar(1,'d', 1, 2, lT)) return
      if(.not.createvar(2,'d', 1, 4, lC)) return
      call UREPRT(CALLS, TIME)
      do 10 i=1,2
         stk(lT+i-1)=TIME(i)
 10   continue
      do 11 i=1,4
         stk(lC+i-1)=CALLS(i)
 11   continue

      lhsvar(1)=1
      lhsvar(2)=2
      return
      end
