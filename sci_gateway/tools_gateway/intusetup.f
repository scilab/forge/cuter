      subroutine intusetup(fname)
c     Author: S. Steer, Copyright INRIA
c     [X,BL,BU]=usetup(file_path)
      include 'stack.h'

      logical getrhsvar,createvar
      logical checklhs,checkrhs
      integer getcuterr

      external getrhsvar,createvar, checklhs,checkrhs
      external getcuterr
      character fname*(*)
      character*8 subname
      character*128 path
      integer prbdat,mode(2)
      COMMON / toolssize / NVAR,M
      save / toolssize /
c
      M=0
      if(.not.checklhs(fname,1,3)) return
      if(.not.checkrhs(fname,1,1)) return
      if(.not.getrhsvar(1,'c', Mp, Np, lP)) return
      mode(1)=1
      mode(2)=0
      path=cstk(lP:lP+min(mp*np-1,127))
      subname='range'//char(0)
      call setrangefonc(subname, ierr)
      if(ierr.ne.0) then
         buf='The range function is not linked'
         call error(997)
         return
      endif
      subname='elfun'//char(0)
      call setelfunfonc(subname, ierr)
      if(ierr.ne.0) then
         buf='The elfun function is not linked'
         call error(997)
         return
      endif
      subname='group'//char(0)
      call setgroupfonc(subname, ierr)
      if(ierr.ne.0) then
         buf='The group function is not linked'
         call error(997)
         return
      endif
      prbdat=0
      call clunit(prbdat,path,mode)
      if(err.gt.0) then
         call error(err)
         return
      endif
c     read the problem dimension to be able to allocate
      nvar=0
      READ(prbdat,'(i8)') NVAR
      rewind(prbdat)
      if(.not.createvar(2,'d', NVAR, 1, lX)) goto 10
      if(.not.createvar(3,'d', NVAR, 1, lBL)) goto 10 
      if(.not.createvar(4,'d', NVAR, 1, lBU)) goto 10 
      CALL USETUP(prbdat, wte, NVAR, stk(lX), stk(lBL), stk(lBU), NVAR)
      ierr=getcuterr()
      if (ierr.ne.0) then
         if (ierr.eq.-1) then
            buf='Overdimensionned problem'
         elseif(ierr.eq.1) then
            buf='the problem includes general constraints'
         elseif(ierr.eq.2) then
            buf='the problem uses no variables'
         elseif(ierr.eq.3) then
            buf='the problem is vacuous'
         endif
            call error(998)
         return
      endif
      lhsvar(1)=2
      lhsvar(2)=3
      lhsvar(3)=4
 10   call clunit(-prbdat,path,mode)
      return
      end
