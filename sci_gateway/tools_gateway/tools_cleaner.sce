mode(-1);
// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
curdir=pwd()
chdir(get_absolute_file_path('tools_cleaner.sce'))
tokeep=[
    'intcreprt.f'
    'intccfg.f'
    'intccfsg.f'
    'intccifg.f'
    'intccifsg.f'
    'intcdh.f'
    'intcsh.f'
    'intcdimen.f'
    'intcdimsh.f'
    'intcdimsj.f'
    'intcfn.f'
    'intcgrdh.f'
    'intcsgrsh.f'
    'intcgr.f'
    'intcidh.f'
    'intcish.f'
    'intcnames.f'
    'intcofg.f'
    'intcprod.f'
    'intcsetup.f'
    'intcvarty.f'
    'intubandh.f'
    'intudh.f'
    'intudimen.f'
    'intudimsh.f'
    'intufn.f'
    'intugrdh.f'
    'intugrsh.f'
    'intugr.f'
    'intunames.f'
    'intuofg.f'
    'intuprod.f'
    'intureprt.f'
    'intusetup.f'
    'intush.f'
    'intuvarty.f'
    'builder.sce'
    'tools_gateway_cleaner.sce'
    'tools_cleaner.sce'];
files=listfiles('*')';
for k=tokeep',files(files==k)=[];end
for f=files,mdelete(f),end
chdir(curdir)
clear files f mdelete listfiles curdir tokeep
