mode(-1);
// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
curdir=pwd()
chdir(get_absolute_file_path('sifdec_cleaner.sce'))
tokeep=[
	'Readme.scilab'
	'sifdec.siz'
	'sifdecerr.f'
        'init.h'
	'builder.sce'
	'sifdec_cleaner.sce'
       'Reference'
       'poub'];
files=listfiles('*')';
for k=tokeep',files(files==k)=[];end
for f=files,mdelete(f),end
chdir(curdir)
clear files f mdelete listfiles curdir tokeep
