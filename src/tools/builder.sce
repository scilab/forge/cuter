
// ====================================================================
// Copyright (C) INRIA - Serge Steer
// Copyright (C) DIGITEO - Allan CORNET
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function build_tools()

mode(-1)
Path=get_absolute_file_path('builder.sce');

//create auxiliary functions
function adapt_from_ref(Path,nam)
  in=Path+'Reference/'+nam;out=Path+nam;sizing=Path+'sizing'
  txt=mgetl(in);
  //selecting the double precision declarations
  txt=strsubst(txt,'/^CDIR/','Cdir','r')
  txt=strsubst(txt,'/^CD/','  ','r')
  txt=strsubst(txt,'/^Cdir/','CDIR','r')
  if nam=='usetup.f'|nam=='csetup.f' then
    //array initialization addition.
     k=grep(txt,'      DEBUG')//find instruction
     k=k(1)-1
     txt=[txt(1:k);'      include ''init.h''';txt(k+1:$)];
  end

  k=grep(txt,'C#{sizing}')
  if k<>[] then
    txt=[txt(1:k);mgetl(sizing);txt(k+1:$)];
  end
  //replacing STOP by a call to setcuterr and a return
  k=grep(txt,'/stop *$/i','r');
  for i=size(k,'*'):-1:1
    ki=k(i); ti=txt(ki)
    ind=1;while part(ti,ind)==' ' do ind=ind+1,end;ind=ind-1
    if convstr(stripblanks(ti))=='stop' then
      txt=[txt(1:ki-1);
	   part(' ',ones(1,ind))+['call setcuterr('''+basename(nam)+''','+string(i)+')'
		    'return']
	   txt(ki+1:$)]
    else
      //      strsubst(txt(ki),'/stop/i','then','r')
      txt=[txt(1:ki-1);
	   strsubst(ti,'STOP','then')
	   part(' ',ones(1,ind+2))+['call setcuterr('''+basename(nam)+''','+string(i)+')'
		    'return']
	   part(' ',ones(1,ind))+'end if'
	   txt(ki+1:$)]
    end
    
  end
  txt=strsubst(txt,' CHARA ',' CCHARA ')
  
  //replace the external routines names 
  //range, elfun and group
  //by
  //rangedr, elfundr, groupdr
  txt=strsubst(txt,'RANGE','RANG1')
  txt=strsubst(txt,'ELFUN','ELFU1')
  txt=strsubst(txt,'GROUP','GROU1')
  mputl(txt,out)
endfunction


  mprintf(_('---------- Building the TOOLS library\n'));
  ncl=lines();lines(0);
  Files=['asmbe.f';'asmbl.f';'ccfg.f';'ccfsg.f';'ccifg.f';'ccifsg.f';'cdh.f';'cdimen.f'; 
	 'cdimse.f';'cdimsh.f';'cdimsj.f';'ceh.f';'cfn.f';'cgr.f';'cgrdh.f';'cidh.f'; 
	 'cish.f';'cnames.f';'cofg.f';'cprod.f';'creprt.f';'cscfg.f';'cscifg.f'; 
	 'csetup.f';'csgr.f';'csgreh.f';'csgrsh.f';'csh.f';'cvarty.f'; 
	 'elgrd.f';'hsprd.f';'initw.f';'others.f';'ubandh.f';'udh.f';'udimen.f'; 
	 'udimse.f';'udimsh.f';'ueh.f';'ufn.f';'ugr.f';'ugrdh.f';'ugreh.f';'ugrsh.f'; 
	 'unames.f';'uofg.f';'uprod.f';'ureprt.f';'usetup.f'; 
	 'ush.f';'uvarty.f']';
  
  if newest(Path+['date_build','builder.sce','Reference/'+Files,'cuterr.f','init.h','sizing','cputim.f','range.c','elfun.c','group.c'])==1 then
    clear Path Files 
    return
  end

  for f=Files, adapt_from_ref(Path,f),end
  //add local files
  Files=[Files,'cuterr.f','cputim.f','range.c','elfun.c','group.c', 'dset.f', 'iset.f'];
  
  //the following lines are a workaround to avoid Scilab-5.1.1 bugs under
  //windows (timer symbol not exported, fortran runtime not
  //shared). These bugs will be fixed in the Scilab-5.2 version
  if getos() == "Windows" then
     Files=[Files 'timer.c'];
     [a,b] = getversion()
     if ( find(b=="release") == [] ) then
       ext_libs = ['libifcoremdd' 'libmmdd'];
     else
       ext_libs = ['libifcoremd' 'libmmd'];
     end
     
  else
    Files=[Files 'timer.c' 'timer.h'];
    ext_libs = [];
  end
  
  try
    tbx_build_src('siftools',Files,'f',Path,ext_libs,"","",'-I'+Path)
    mputl(sci2exp(getdate()),Path+'date_build')
  catch
    mprintf("%s\n",lasterror())
  end
  lines(ncl(2))
endfunction

build_tools();
clear build_tools;

