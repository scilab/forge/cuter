/*====================================================================
 * Copyright (C) INRIA -  Serge Steer
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http:*www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 * ====================================================================*/
/* This is a driver able to define and call a dynamically linked  "elfun" subroutine */

#include "machine.h"
#include "version.h"

/* next definition taken from AddFunctionInTable.h */
typedef void (*voidf)();
typedef struct {
	char *name; /* function name */
	voidf f; /* pointer on function */
} FTAB;

extern voidf AddFunctionInTable (char *name, int *rep, FTAB *table);  

#if (SCI_VERSION_MAJOR >= 5) && (SCI_VERSION_MINOR >= 3)
  extern voidf GetFunctionByName (char *name, int *rep, FTAB *table);
	#define AddFunctionInTable GetFunctionByName
#endif

#define ARGS_elfun double *fuvals, double *xvalue, double *epvalu, int *ncalcf, \
    int *itypee, int *istaev, int *ielvar, int *intvar, int *istadh, \
    int *istepa, int *icalcf, int *ltypee, int *lstaev, int *lelvar, \
    int *lntvar, int *lstadh, int *lstepa, int *lcalcf, int *lfvalu, \
    int *lxvalu, int *lepvlu, int *ifflag, int *ifstat

FTAB FTab_elfun[] =
{
	{(char *) 0, (voidf) 0}
};

typedef int * (*elfunf)(ARGS_elfun);

/** the current function fixed by setelfunfonc **/
static elfunf elfunfonc ; /** the current function fixed by setelfunfonc **/

void C2F(setelfunfonc)(char *name, int *rep)
{
  elfunfonc=(elfunf) AddFunctionInTable(name,rep,FTab_elfun);
}

int C2F(elfu1)(ARGS_elfun)
{
  (*elfunfonc)(fuvals, xvalue, epvalu, ncalcf, itypee, istaev,
	       ielvar, intvar, istadh, istepa, icalcf, ltypee, lstaev, lelvar,
	       lntvar, lstadh, lstepa, lcalcf, lfvalu, lxvalu, lepvlu, ifflag,
	       ifstat);
  return 0;
}
