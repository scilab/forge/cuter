/*====================================================================
 * Copyright (C) INRIA -  Serge Steer
 * 
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at    
 * http:*www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 * ====================================================================*/

/* This is a driver able to define and call a dynamically linked "range" subroutine */
#include "machine.h"
#include "version.h"

/* next definition taken from AddFunctionInTable.h */
typedef void (*voidf)();

typedef struct {
	char *name; /* function name */
	voidf f; /* pointer on function */
} FTAB;

extern voidf AddFunctionInTable (char *name, int *rep, FTAB *table);

#if (SCI_VERSION_MAJOR >= 5) && (SCI_VERSION_MINOR >= 3)
  extern voidf GetFunctionByName (char *name, int *rep, FTAB *table);
	#define AddFunctionInTable GetFunctionByName
#endif


#define ARGS_range int *ielemn, int *transp, double *w1, double *w2,int *nelvar, int *ninvar, \
    int *itype,int *lw1, int *lw2


FTAB FTab_range[] =
{
	{(char *) 0, (voidf) 0}
};

typedef int * (*rangef)(ARGS_range);

/** the current function fixed by setrangefonc **/
static rangef rangefonc ; /** the current function fixed by setrangefonc **/

void C2F(setrangefonc)(char *name, int *rep)
{
  rangefonc=(rangef) AddFunctionInTable(name,rep,FTab_range);
}

int C2F(rang1)(ARGS_range)
{
  (*rangefonc)(ielemn, transp, w1, w2,nelvar, ninvar, itype, lw1, lw2);
  return 0;
}
 