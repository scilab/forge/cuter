mode(-1);
// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
curdir=pwd()
chdir(get_absolute_file_path('tools_cleaner.sce'))
tokeep=[
    'README'
    'cuterr.f'
    'cputim.f'
    'range.c'
    'elfun.c'
    'group.c'
    'init.h'
    'sizing'
    'builder.sce'
    'tools_cleaner.sce'
    'Reference'
    'timer.c'
    'timer.h'  ];//this later file is a modified version of the Scilab one, 
files=listfiles('*')';
for k=tokeep',files(files==k)=[];end
for f=files,mdelete(f),end
chdir(curdir)
clear files f mdelete listfiles curdir tokeep
