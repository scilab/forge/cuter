// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
function [f,g,ind]=ARGLINB_optim(x,ind)
//external for optim
//   minimize: sum {i in 1..M} ( sum {j in 1..N} x[j]*i*j -1.0)^2;
  global count;
  if or(ind==[2 4]) then 
    count(1)=count(1)+1
  elseif  or(ind==[3 4]) then 
     count(2)=count(2)+1
  end
  m=20;//number of equation
  n=size(x,'*')
  A=((1:m)'*ones(1,n)).*(ones(m,1)*(1:n))
  y=A*x-ones(m,1)
  f=y'*y
  g=2*(A'*A*x-A'*ones(m,1))
endfunction

function [y]=ARGLINB_cost(x,m)
  //external for lsqrsolve
  global count;  count(1)=count(1)+1
  n=size(x,'*')
  A=((1:m)'*ones(1,n)).*(ones(m,1)*(1:n))
  y=A*x-ones(m,1)
endfunction


function [J]=ARGLINB_gradient(x,m)
   //external for lsqrsolve
   global count;  count(2)=count(2)+1
  n=size(x,'*')
  A=((1:m)'*ones(1,n)).*(ones(m,1)*(1:n))
  J=A
endfunction


count=[0 0]
[xopt,v]=lsqrsolve(ones(10,1),ARGLINB_cost,20,ARGLINB_gradient,[1d-13,1d-13,1d-14,500,%eps,1]);
count
fopt=v'*v
[fopt,gopt]=ARGLINB(xopt,4);fopt,norm(gopt)


count=[0 0]
xopt=lsqrsolve(ones(10,1),ARGLINB_cost,20,[1d-13,1d-13,1d-14,500,%eps,1]);
count
[fopt,gopt]=ARGLINB(xopt,4);fopt,norm(gopt)



//fminsearch

function [y,index]=ARGLINB_cost(x,index)
  m=20
  global count;  count(1)=count(1)+1
  x=x(:)
  n=size(x,'*')
  A=((1:m)'*ones(1,n)).*(ones(m,1)*(1:n))
  y=A*x-ones(m,1)
  y=y'*y
endfunction

