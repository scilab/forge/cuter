// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

function [f,g,ind]=BDEXP(x,ind)
  //A Scilab transcription of the BDEXP.SIF problem for optim

  //the CUTEr BDEXP problem, n must be greater or equal to 4
    global count
    n=size(x,1);
    g=zeros(x);
    if or(ind==[2 4]) then//the function
      count(1)=count(1)+1;
      f=sum((x(1:n-2)+x(2:n-1)).*exp(-(x(1:n-2)+x(2:n-1)).*x(3:n)))
    end
    if or(ind==[3 4]) then//the gradient
      count(2)=count(2)+1;
  
      g1=(1-(x(1)+x(2)).*x(3)).*exp(-(x(1)+x(2)).*x(3));//diff par rapport a x1
      g2=(1-(x(1)+x(2)).*x(3)).*exp(-(x(1)+x(2)).*x(3))+(1-(x(2)+x(3)).*x(4)).*exp(-(x(2)+x(3)).*x(4));//diff par rapport a x2
 
      if n>4 then
	gi=(1-(x(2:n-3)+x(3:n-2)).*x(4:n-1)).*exp(-(x(2:n-3)+x(3:n-2)).*x(4:n-1))....
	   +(1-(x(3:n-2)+x(4:n-1)).*x(5:n)).*exp(-(x(3:n-2)+x(4:n-1)).*x(5:n))....
	   -(x(1:n-4)+x(2:n-3))^(2).*exp(-(x(1:n-4)+x(2:n-3)).*x(3:n-2));
      else
	gi=[]
      end
      gnm1= +(1-(x(n-2)+x(n-1)).*x(n)).*exp(-(x(n-2)+x(n-1)).*x(n))-(x(n-3)+x(n-2))^(2).*exp(-(x(n-3)+x(n-2)).*x(n-1));
      gn=-(x(n-2)+x(n-1))^(2).*exp(-(x(n-2)+x(n-1)).*x(n));
      g=[g1;g2;gi;gnm1;gn];
    end
endfunction
