mode(-1)
// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

siffile=get_sif_path()+'sif/BDQRTIC.SIF';
[fopt,xopt,gopt]=sifoptim(siffile,'qn',Stop=list(500,500,1d-10,1d-10));



function [f,g,ind]=BDQRTIC(x,ind)
//  external for optim
//the CUTEr BDQRTIC problem, n must be greater or equal to 4
  global count
  n=size(x,1);
  g=zeros(x);
  if or(ind==[2 4]) then
    count(1)=count(1)+1;
    i=1:n-4;
    f=sum( (-4*x(i)+3).^2+(x(i).^2+2*x(i+1).^2+3*x(i+2).^2+4*x(i+3).^2+5*x(n).^2).^2 );
  end
  if or(ind==[3 4]) then
    count(2)=count(2)+1;
    i=1;
    g1  =  32*x(i)-24+...
	   (4*(x(i).^2+  2*x(i+1).^2+3*x(i+2).^2+4*x(i+3).^2+5*x(n).^2)).*x(i);

    i=2;
    g2  =  32*x(i)-24+...
	   (8*(x(i-1).^2+2*x(i).^2  +3*x(i+1).^2+4*x(i+2).^2+5*x(n).^2)+...
	    4*(x(i).^2+  2*x(i+1).^2+3*x(i+2).^2+4*x(i+3).^2+5*x(n).^2)).*x(i);
    i=3;
    g3  = 32*x(i)-24+...
	  (12*(x(i-2).^2+2*x(i-1).^2+3*x(i).^2  +4*x(i+1).^2+5*x(n).^2)+...
	   8*(x(i-1).^2+2*x(i).^2  +3*x(i+1).^2+4*x(i+2).^2+5*x(n).^2)+...
	   4*(x(i).^2+  2*x(i+1).^2+3*x(i+2).^2+4*x(i+3).^2+5*x(n).^2)).*x(i);
    
    i=4:n-4;
    gi  = 32*x(i)-24+...
	  (16*(x(i-3).^2+2*x(i-2).^2+3*x(i-1).^2+4*x(i).^2  +5*x(n).^2)+...
	   12*(x(i-2).^2+2*x(i-1).^2+3*x(i).^2  +4*x(i+1).^2+5*x(n).^2)+...
	    8*(x(i-1).^2+2*x(i).^2  +3*x(i+1).^2+4*x(i+2).^2+5*x(n).^2)+...
	    4*(x(i).^2+  2*x(i+1).^2+3*x(i+2).^2+4*x(i+3).^2+5*x(n).^2)).*x(i);
    i=n-3;
    gnm3 =(16*(x(i-3).^2+2*x(i-2).^2+3*x(i-1).^2+4*x(i).^2  +5*x(n).^2)+...
	   12*(x(i-2).^2+2*x(i-1).^2+3*x(i).^2  +4*x(i+1).^2+5*x(n).^2)+...
	   8*(x(i-1).^2+2*x(i).^2  +3*x(i+1).^2+4*x(i+2).^2+5*x(n).^2)).*x(i);
    
    i=n-2;
    gnm2 =(16*(x(i-3).^2+2*x(i-2).^2+3*x(i-1).^2+4*x(i).^2  +5*x(n).^2)+...
	    12*(x(i-2).^2+2*x(i-1).^2+3*x(i).^2  +4*x(i+1).^2+5*x(n).^2)).*x(i);
    i=n-1;
    gnm1 = (16*(x(i-3).^2+2*x(i-2).^2+3*x(i-1).^2+4*x(i).^2  +5*x(n).^2)).*x(i);
     
    i=1:n-4;
    gn   = (100*n-400)*x(n).^3+20*x(n).*sum(x(i).^2+2*x(i+1).^2+3*x(i+2).^2+4*x(i+3).^2);

    g=[g1;g2;g3;gi;gnm3;gnm2;gnm1;gn];
  end
endfunction
Stop=[500;500;1d-10;1d-10];
n=100;
global count;count=[0,0];
[f1,x1,g1]=optim(BDQRTIC,ones(n,1),'qn','ar',Stop(1),Stop(2),Stop(3), ...
		 Stop(4),1d-12*ones(n,1),imp=0);

disp(f1,fopt)
