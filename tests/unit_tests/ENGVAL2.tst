// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
//testing unconstraint problem ENGVAL2 (uncontraint non linear problem)
//the function which computes the function, the gradient and the jacobian
function y=F(x)
  y=(x(1)^2+x(2)^2+x(3)^2-1)^2 ..
    + (x(1)^2+x(2)^2+(x(3)-2)^2-1)^2 ..
    + (x(1)+x(2)+x(3)-1)^2 ..
    + (x(1)+x(2)-x(3)+1)^2 ..
    + (3*x(2)^2+x(1)^3+(5*x(3)-x(1)+1)^2-36)^2
endfunction	
function dy=dF(x)
  x1=x(1);x2=x(2);x3=x(3);
  dy=[4*(x1^2+x2^2+x3^2-1)*x1+4*(x1^2+x2^2+(x3-2)^2-1)*x1+ 4*x1+4*x2+2*(3*x2^2+x1^3+(5*x3-x1+1)^2-36)*(3*x1^2-10*x3+2*x1-2)
      4*(x1^2+x2^2+x3^2-1)*x2+4*(x1^2+x2^2+(x3-2)^2-1)*x2+ 4*x1+4*x2+12*(3*x2^2+x1^3+(5*x3-x1+1)^2-36)*x2
      4*(x1^2+x2^2+x3^2-1)*x3+2*(x1^2+x2^2+(x3-2)^2-1)*(2*x3-4)+4*x3-4+2*(3* x2^2+x1^3+(5*x3-x1+1)^2-36)*(50*x3-10*x1+10)]
endfunction	

function d2y=d2F(x)
  x1=x(1);x2=x(2);x3=x(3);
  d2y=zeros(3,3)
  d2y(1,1)=24*x1^2+8*x2^2+4*x3^2-4+4*(x3-2)^2+2*(3*x1^2-10*x3+2*x1-2)^2+2*(3*x2^2+x1^3+(5*x3-x1+1)^2-36)*(6*x1+2)
  d2y(1,2)= 16*x2*x1+4+12*x2*(3*x1^2-10*x3+2*x1-2)
  d2y(1,3)= 8*x3*x1+4*(2*x3-4)*x1+2*(50*x3-10*x1+10)*(3*x1^2-10*x3+2*x1-2)-60*x2^2-20*x1^3-20*(5*x3-x1+1)^2+720

  d2y(2,1)= d2y(1,2)
  d2y(2,2)= 132*x2^2+8*x1^2+4*x3^2-436+4*(x3 -2)^2+12*x1^3+12*(5*x3-x1+1)^2
  d2y(2,3)= 8*x3*x2+4*(2*x3-4)*x2+12*(50*x3-10*x1+10)*x2

  d2y(3,1)=d2y(1,3)
  d2y(3,2)=d2y(2,3)
  d2y(3,3)=12*x3^2+8*x1^2+308*x2^2-3604+2*(2*x3-4)^2+4*(x3-2)^2+2*(50*x3-10*x1+10)^2+100*x1^3+100*(5*x3-x1+1)^2
endfunction	
x0=[1;2;0];
function A=CuteSparse(n,H,ICNH,IRNH)
  k=find(ICNH<>IRNH)
  A=sparse([[ICNH;IRNH(k)],[IRNH;ICNH(k)]],[H;H(k)],[n,n])
endfunction
path=get_sif_path()+'sif';
siffile=path+'/ENGVAL2.SIF';
probpath=TMPDIR+'/ENGVAL2';
sifdecode(siffile,probpath) //routines are created in the Scilab temporary directory
buildprob(probpath)
[x,bl,bu] = usetup(probpath+'/OUTSDIF.d');
if or(x<>x0) then pause,end
if or(size(bl)<>[3 1]) then pause,end
if or(size(bu)<>[3 1]) then pause,end
if udimen()<>3 then pause,end
if udimsh()<>6 then pause,end
if unames()<>"ENGVAL2   " then pause,end
[PNAME,VNAME] = unames();
if PNAME<>"ENGVAL2   " then pause,end
if VNAME<>"X1        X2        X3        "  then pause,end


if ufn(x)<>F(x)  then pause,end
if or(ugr(x)<>dF(x))  then pause,end
if or(udh(x)<>d2F(x)) then pause,end
[G,H] = ugrdh(x);
if or(G<>dF(x))  then pause,end
if or(H<>d2F(x))  then pause,end
[G,SH,ICNH,IRNH] = ugrsh(x);
if or(G<>dF(x))  then pause,end
if or(full(CuteSparse(3,SH(:),ICNH(:),IRNH(:)))<>d2F(x))  then pause,end
[f,G] = uofg(x);
if f<>F(x)|or(G<>dF(x))  then pause,end
if or(uprod(x,[1;1;1],%t)<>d2F(x)*[1;1;1])  then pause,end
if or(uprod(x,[1;1;1],%f)<>d2F(x)*[1;1;1])  then pause,end
[SH,ICNH,IRNH] = ush(x);
if or(full(CuteSparse(3,SH(:),ICNH(:),IRNH(:)))<>d2F(x))  then pause,end
if or(uvarty()<>zeros(3,1)) then pause,end

Stop=[500;500;1d-14;1d-14];
[fopt,xopt,gopt]=sifoptim(siffile,'qn',Stop);
if fopt<>0 then pause,end
if norm(xopt-[0;0;1])>1d-14 then pause,end
