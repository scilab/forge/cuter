function [f,g,h]=ARGLINB(x)
%   minimize: sum {i in 1..M} ( sum {j in 1..N} x[j]*i*j -1.0)^2;
  m=20;%number of equation
  global count;

  count(1)=count(1)+1;
  if nargout>=2 ;
    count(2)=count(2)+1;
  end


  n=size(x,1);
  A=((1:m)'*ones(1,n)).*(ones(m,1)*(1:n));
  y=A*x-ones(m,1);
  f=y'*y;
  g=2*(A'*A*x-A'*ones(m,1));
  h=[];
  

