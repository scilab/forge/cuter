options=optimset('fminunc');
options.TolFun=1e-11;
options.TolX=1e-12;
options.GradObj='on';
options.LargeScale='off'
global count;count=[0 0];
[xopt,fopt,info,output,gopt]=fminunc('ARGLINB',ones(10,1),options);
fprintf(1,'cost=%e, gradient norm=%e, cost evaluation=%d, gradient evaluation=%d\n',fopt,norm(gopt),count(1),count(2))
