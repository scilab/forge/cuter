options=optimset('fminunc');
options.TolFun=1e-13;
options.TolX=1e-12;
options.GradObj='on';


options.LargeScale='off';
global count;count=[0 0];
[xopt,fopt,info,output,gopt]=fminunc('BDEXP',ones(100,1),options);
fprintf(1,'LargeScale=''off'',cost=%e, gradient norm=%e, cost evaluation=%d, gradient evaluation=%d\n',fopt,norm(gopt),count(1),count(2))
options.LargeScale='on';
global count;count=[0 0];
[xopt,fopt,info,output,gopt]=fminunc('BDEXP',ones(100,1),options);
fprintf(1,'LargeScale=''on'',cost=%e, gradient norm=%e, cost evaluation=%d, gradient evaluation=%d\n',fopt,norm(gopt),count(1),count(2))
