options=optimset('fminunc');
options.TolFun=1e-11;
options.TolX=1e-12;
options.GradObj='on';

options.LargeScale='off';
n=1000;
global count;count=[0 0];
[xopt,fopt,info,output,gopt]=fminunc('BDQRTIC',ones(n,1),options);
fprintf(1,'fminunc, LargeScale=''off'',cost=%e, gradient norm=%e, cost evaluation=%d, gradient evaluation=%d\n',fopt,norm(gopt),count(1),count(2))
%LargeScale='off',cost=3.983818e+03, gradient norm=9.801076e-05, cost evaluation=218, gradient evaluation=218


%% very inefficient method
options.LargeScale='on';
n=1000;
global count;count=[0 0];
[xopt,fopt,info,output,gopt]=fminunc('BDQRTIC',ones(n,1),options);
fprintf(1,'fminunc, LargeScale=''on'',cost=%e, gradient norm=%e, cost evaluation=%d, gradient evaluation=%d\n',fopt,norm(gopt),count(1),count(2))
%LargeScale='on',cost=3.983818e+03, gradient norm=7.900292e-06, cost evaluation=12012, gradient evaluation=12012



options=optimset('fminsearch');
options.TolFun=1e-8;
options.TolX=1e-8;
options.MaxFunEvals= 40000;
  options.MaxIter= 20000;
n=100;
global count;count=0;
[xopt,fopt,info,output]=fminsearch('BDQRTIC',ones(n,1),options);
fprintf(1,'fminsearch, cost=%e,  cost evaluation=%d\n',fopt,count(1))
