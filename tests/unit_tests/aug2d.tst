// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

//This script applies the basic CUTEr functions to the AUG2D.SIF problem.

siffile=get_sif_path()+'sif/AUG2D.SIF';
probpath=TMPDIR+'/AUG2D'
sifdecode(siffile,probpath) //routines are created in the Scilab temporary directory
buildprob(probpath)
[x,bl,bu,v,cl,cu,equatn,linear] = csetup(probpath+'/OUTSDIF.d', [%t %t %f]);
[v,C] = ccfg(x);
b=v-C*x;
me=size(find(equatn),'*')
