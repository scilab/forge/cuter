mode(-1)
// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

//This scripts tries to compile  all SIF problems given in the sif
//directory and to generate the corresponding dynamic library. 
//The decoded problems are supposed to be stored in the
//<TMPDIR>/cute_problems by a preview run of the decodeall.sce script

path=get_sif_path()+'sif';
//list the files in the sif directory
PBS=stripblanks(basename(listfiles(path+'/*.SIF')));
//sort the names
PBS=gsort(PBS,'g','i');
wmode=warning("query");warning("off");
curdir=pwd();
ROOT=TMPDIR;
//ROOT='/tmp';

out=mopen(ROOT+'/cute_problems/build.log','w');
for k=1:size(PBS,'*')
  mprintf("%s\n" ,PBS(k));
  Path=ROOT+'/cute_problems/'+PBS(k);
  chdir(Path);

  Files=['RANGE.f','GROUP.f','ELFUN.f'];
  Entries=['range','group','elfun'];
  ierr=execstr('libprob=ilib_for_link(Entries,Files,[],''f'')', ...
	       'errcatch');
  if ierr<>0 then 
    txt=lasterror();
    mprintf("%s (%d) failed %s\n",PBS(k),k,txt);
    mfprintf(out,"%s (%d) failed %s\n",PBS(k),k,txt);
  end
end
chdir(curdir);
mclose(out)
