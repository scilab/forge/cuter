mode(-1)
// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

//This scripts tries to decode all SIF problems given in the sif
//directory. The decoded problems are stored in the
//<TMPDIR>/cute_problems (one subdirectory for each problem)

path=get_sif_path()+'sif';
//list the files in the sif directory
PBS=stripblanks(basename(listfiles(path+'/*.SIF')));
//sort the names
PBS=gsort(PBS,'g','i');
ROOT=TMPDIR;
//ROOT='/tmp';

mkdir(ROOT,'cute_problems');
out=mopen(ROOT+'/cute_problems/decode.log','w');

for k=1:size(PBS,'*')
  mprintf("%s\n",PBS(k))
  pin=path+'/'+PBS(k)+'.SIF';
  pout=ROOT+'/cute_problems/'+PBS(k);
  ierr=execstr('sifdecode(pin,pout)','errcatch');
  if ierr<>0 then 
    txt=lasterror();
    mprintf("%s (%d) failed %s\n",PBS(k),k,txt);
    mfprintf(out,"%s (%d) failed %s\n",PBS(k),k,txt);
  end
end
