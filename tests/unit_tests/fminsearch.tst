// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================
//to test fminserach against the ARGLINB and BDQRTIC problems
function [f]=ARGLINB_cost(x)
  global count;  count(1)=count(1)+1
  m=20;//number of equations
  n=size(x,'*')
  A=((1:m)'*ones(1,n)).*(ones(m,1)*(1:n))
  y=A*x-ones(m,1)
  f=y'*y
endfunction

function g=ARGLINB_gradient(x)
  n=size(x,'*')
  m=20;//number of equations
  A=((1:m)'*ones(1,n)).*(ones(m,1)*(1:n))
  g=A
endfunction

options=optimset('fminsearch')
options.TolFun=1e-13;
options.TolX=1e-13;

global count;count=0;
[xopt,fopt,exitflag,output]= fminsearch (ARGLINB_cost,ones(10,1) );xopt=xopt';
  gopt=ARGLINB_gradient(xopt);
 
mprintf('algo=''fminsearch'',cost=%e, gradient norm=%e, cost evaluation=%d\n',fopt,norm(gopt),count(1))
//algo='fminsearch',cost=4.634173e+00, gradient norm=1.051166e+03, cost evaluation=392

function f=BDQRTIC_cost(x)
//the CUTEr BDQRTIC problem, n must be greater or equal to 4
  global count
  n=size(x,1);
  count(1)=count(1)+1;
  i=1:n-4;
  f=sum( (-4*x(i)+3).^2+(x(i).^2+2*x(i+1).^2+3*x(i+2).^2+4*x(i+3).^2+5*x(n).^2).^2 );
endfunction
function g=BDQRTIC_gradient(x)
//the CUTEr BDQRTIC problem, n must be greater or equal to 4
  n=size(x,1);

    i=1;
    g1  =  32*x(i)-24+...
	   (4*(x(i).^2+  2*x(i+1).^2+3*x(i+2).^2+4*x(i+3).^2+5*x(n).^2)).*x(i);

    i=2;
    g2  =  32*x(i)-24+...
	   (8*(x(i-1).^2+2*x(i).^2  +3*x(i+1).^2+4*x(i+2).^2+5*x(n).^2)+...
	    4*(x(i).^2+  2*x(i+1).^2+3*x(i+2).^2+4*x(i+3).^2+5*x(n).^2)).*x(i);
    i=3;
    g3  = 32*x(i)-24+...
	  (12*(x(i-2).^2+2*x(i-1).^2+3*x(i).^2  +4*x(i+1).^2+5*x(n).^2)+...
	   8*(x(i-1).^2+2*x(i).^2  +3*x(i+1).^2+4*x(i+2).^2+5*x(n).^2)+...
	   4*(x(i).^2+  2*x(i+1).^2+3*x(i+2).^2+4*x(i+3).^2+5*x(n).^2)).*x(i);
    
    i=4:n-4;
    gi  = 32*x(i)-24+...
	  (16*(x(i-3).^2+2*x(i-2).^2+3*x(i-1).^2+4*x(i).^2  +5*x(n).^2)+...
	   12*(x(i-2).^2+2*x(i-1).^2+3*x(i).^2  +4*x(i+1).^2+5*x(n).^2)+...
	    8*(x(i-1).^2+2*x(i).^2  +3*x(i+1).^2+4*x(i+2).^2+5*x(n).^2)+...
	    4*(x(i).^2+  2*x(i+1).^2+3*x(i+2).^2+4*x(i+3).^2+5*x(n).^2)).*x(i);
    i=n-3;
    gnm3 =(16*(x(i-3).^2+2*x(i-2).^2+3*x(i-1).^2+4*x(i).^2  +5*x(n).^2)+...
	   12*(x(i-2).^2+2*x(i-1).^2+3*x(i).^2  +4*x(i+1).^2+5*x(n).^2)+...
	   8*(x(i-1).^2+2*x(i).^2  +3*x(i+1).^2+4*x(i+2).^2+5*x(n).^2)).*x(i);
    
    i=n-2;
    gnm2 =(16*(x(i-3).^2+2*x(i-2).^2+3*x(i-1).^2+4*x(i).^2  +5*x(n).^2)+...
	    12*(x(i-2).^2+2*x(i-1).^2+3*x(i).^2  +4*x(i+1).^2+5*x(n).^2)).*x(i);
    i=n-1;
    gnm1 = (16*(x(i-3).^2+2*x(i-2).^2+3*x(i-1).^2+4*x(i).^2  +5*x(n).^2)).*x(i);
     
    i=1:n-4;
    gn   = (100*n-400)*x(n).^3+20*x(n).*sum(x(i).^2+2*x(i+1).^2+3*x(i+2).^2+4*x(i+3).^2);

    g=[g1;g2;g3;gi;gnm3;gnm2;gnm1;gn];
endfunction


options=optimset('fminsearch')
options.TolFun=1e-13;
options.TolX=1e-13;
options.MaxIter=20000;
options.MaxFunEvals=20000;

global count;count=0;
[xopt,fopt,exitflag,output]= fminsearch (BDQRTIC_cost,ones(100,1) );xopt=xopt';
  gopt=BDQRTIC_gradient(xopt);
 
mprintf('algo=''fminsearch'',cost=%e, gradient norm=%e, cost evaluation=%d\n',fopt,norm(gopt),count(1))
