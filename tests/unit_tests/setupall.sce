mode(-1)
// ====================================================================
// Copyright (C) INRIA -  Serge Steer
// 
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at    
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
// ====================================================================

//This scripts tries to apply the csetup function to  all SIF problems given in the sif
//directory. 
//The decoded problems and their associated dynamic library are supposed to be stored in the
//<TMPDIR>/cute_problems by a preview run of the decodeall.sce and build
//all scripts

path=get_sif_path()+'sif';
//list the files in the sif directory
PBS=stripblanks(basename(listfiles(path+'/*.SIF')));
//sort the names
PBS=gsort(PBS,'g','i');
ROOT=TMPDIR;
//ROOT='/tmp';
out=mopen(ROOT+'/cute_problems/setup.log','w');
for k=293:size(PBS,'*')
  mprintf("%s\n" ,PBS(k));
  Path=ROOT+'/cute_problems/'+PBS(k);
  Entries=['range','group','elfun'];
  ierr=execstr('l1=link(Path+""/librange.so"",Entries)','errcatch');
  if ierr<>0 then 
    txt=lasterror();
    mprintf("%s (%d) link failed %s\n",PBS(k),k,txt);
    mfprintf(out,"%s (%d) link failed %s\n",PBS(k),k,txt);
  end
  ierr=execstr('[XSTART,bl,bu,v,cl,cu,equatn,linear] = csetup(Path+''/OUTSDIF.d'',[%t %t %f]);','errcatch');
  ulink(l1);
  if ierr<>0 then 
    txt=lasterror();
    mprintf("%s (%d) setup failed %s\n",PBS(k),k,txt);
    mfprintf(out,"%s (%d) setup failed %s\n",PBS(k),k,txt);
  end
end
mclose(out)
